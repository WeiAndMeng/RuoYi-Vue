package com.ruoyi.common.enums;

public enum ResumeSubmissionStatus {
    PENDING("待处理"),      // 投递状态: 待处理
    INTERVIEWING("面试中"),  // 投递状态: 面试中
    HIRED("已录用"),        // 投递状态: 已录用
    REJECTED("未录用");     // 投递状态: 未录用

    private final String label;

    // 构造方法
    ResumeSubmissionStatus(String label) {
        this.label = label;
    }

    // 获取对应的中文描述
    public String getLabel() {
        return label;
    }

    // 根据枚举获取枚举详情
    public static ResumeSubmissionStatus fromLabel(String name) {
        for (ResumeSubmissionStatus status : values()) {
            if (status.name().equals(name)) {
                return status;
            }
        }
        return null;
    }
}
