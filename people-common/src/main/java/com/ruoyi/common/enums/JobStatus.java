package com.ruoyi.common.enums;

public enum JobStatus {
    PENDING("待审核"),
    APPROVED("审核通过"),
    REJECTED("审核拒绝")
    ;

    private final String description;

    JobStatus(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    /**
     * 根据状态值获取对应的枚举实例
     */
    public static JobStatus fromValue(String value) {
        for (JobStatus status : values()) {
            if (status.name().equals(value)) {
                return status;
            }
        }
        return null;
    }
}
