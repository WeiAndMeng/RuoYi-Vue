package com.ruoyi.framework.config;

import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.io.IOException;

@org.springframework.context.annotation.Configuration
public class FreemarkerConfig {

    @Bean
    @Primary
    public Configuration freemarkerConfiguration() throws IOException {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_31);
        
        // 设置模板文件所在的目录
        cfg.setClassLoaderForTemplateLoading(getClass().getClassLoader(), "templates");

        // 设置编码
        cfg.setDefaultEncoding("UTF-8");
        
        // 设置异常处理策略
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setLogTemplateExceptions(false);
        cfg.setWrapUncheckedExceptions(true);
        
        // 返回配置
        return cfg;
    }
}
