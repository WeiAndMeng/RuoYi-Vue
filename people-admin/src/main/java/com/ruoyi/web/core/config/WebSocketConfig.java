package com.ruoyi.web.core.config;

import com.ruoyi.web.controller.chat.WebSocketHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        // 注册 WebSocket 处理器，路由路径为 /chat/{chatId}
        registry.addHandler(new WebSocketHandler(), "/chat/{chatId}")
                .setAllowedOrigins("*"); // 允许所有来源连接
    }
}
