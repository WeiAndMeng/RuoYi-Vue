package com.ruoyi.web.controller.business.resume.cutomer;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.Resumes;
import com.ruoyi.system.service.IResumesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ResumeExperiences;
import com.ruoyi.system.service.IResumeExperiencesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 工作经历Controller
 * 
 * @author ruoyi
 * @date 2025-01-17
 */
@RestController
@RequestMapping("/resume/experiences")
public class ResumeExperiencesController extends BaseController
{
    @Autowired
    private IResumeExperiencesService resumeExperiencesService;

    @Autowired
    private IResumesService resumesService;

    /**
     * 查询工作经历列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ResumeExperiences resumeExperiences)
    {
        startPage();
        Resumes resumes = resumesService.getByLoginUserId(SecurityUtils.getUserId());
        if (resumes == null) {
            throw new ServiceException("请先维护简历信息！");
        }
        resumeExperiences.setResumeId(resumes.getResumeId());
        List<ResumeExperiences> list = resumeExperiencesService.selectResumeExperiencesList(resumeExperiences);
        return getDataTable(list);
    }

    /**
     * 导出工作经历列表
     */
    @Log(title = "工作经历", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ResumeExperiences resumeExperiences)
    {
        List<ResumeExperiences> list = resumeExperiencesService.selectResumeExperiencesList(resumeExperiences);
        ExcelUtil<ResumeExperiences> util = new ExcelUtil<ResumeExperiences>(ResumeExperiences.class);
        util.exportExcel(response, list, "工作经历数据");
    }

    /**
     * 获取工作经历详细信息
     */
    @GetMapping(value = "/{experienceId}")
    public AjaxResult getInfo(@PathVariable("experienceId") Long experienceId)
    {
        return success(resumeExperiencesService.selectResumeExperiencesByExperienceId(experienceId));
    }

    /**
     * 新增工作经历
     */
    @Log(title = "工作经历", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ResumeExperiences resumeExperiences)
    {
        Resumes resumes = resumesService.getByLoginUserId(SecurityUtils.getUserId());
        if (resumes == null) {
            throw new ServiceException("请先维护简历信息！");
        }
        resumeExperiences.setResumeId(resumes.getResumeId());
        return toAjax(resumeExperiencesService.insertResumeExperiences(resumeExperiences));
    }

    /**
     * 修改工作经历
     */
    @Log(title = "工作经历", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ResumeExperiences resumeExperiences)
    {
        return toAjax(resumeExperiencesService.updateResumeExperiences(resumeExperiences));
    }

    /**
     * 删除工作经历
     */
    @Log(title = "工作经历", businessType = BusinessType.DELETE)
	@DeleteMapping("/{experienceIds}")
    public AjaxResult remove(@PathVariable Long[] experienceIds)
    {
        return toAjax(resumeExperiencesService.deleteResumeExperiencesByExperienceIds(experienceIds));
    }
}
