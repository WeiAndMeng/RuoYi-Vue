package com.ruoyi.web.controller.business.resume.cutomer;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.Resumes;
import com.ruoyi.system.service.IResumesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ResumeEducation;
import com.ruoyi.system.service.IResumeEducationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 教育背景Controller
 * 
 * @author ruoyi
 * @date 2025-01-17
 */
@RestController
@RequestMapping("/resume/education")
public class ResumeEducationController extends BaseController
{
    @Autowired
    private IResumeEducationService resumeEducationService;

    @Autowired
    private IResumesService resumesService;

    /**
     * 查询教育背景列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ResumeEducation resumeEducation)
    {
        startPage();
        Resumes resumes = resumesService.getByLoginUserId(SecurityUtils.getUserId());
        if (resumes == null) {
            throw new ServiceException("请先维护简历信息！");
        }
        resumeEducation.setResumeId(resumes.getResumeId());
        List<ResumeEducation> list = resumeEducationService.selectResumeEducationList(resumeEducation);
        return getDataTable(list);
    }

    /**
     * 导出教育背景列表
     */
    @Log(title = "教育背景", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ResumeEducation resumeEducation)
    {
        List<ResumeEducation> list = resumeEducationService.selectResumeEducationList(resumeEducation);
        ExcelUtil<ResumeEducation> util = new ExcelUtil<ResumeEducation>(ResumeEducation.class);
        util.exportExcel(response, list, "教育背景数据");
    }

    /**
     * 获取教育背景详细信息
     */
    @GetMapping(value = "/{educationId}")
    public AjaxResult getInfo(@PathVariable("educationId") Long educationId)
    {
        return success(resumeEducationService.selectResumeEducationByEducationId(educationId));
    }

    /**
     * 新增教育背景
     */
    @Log(title = "教育背景", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ResumeEducation resumeEducation)
    {
        Resumes resumes = resumesService.getByLoginUserId(SecurityUtils.getUserId());
        if (resumes == null) {
            throw new ServiceException("请先维护简历信息！");
        }
        resumeEducation.setResumeId(resumes.getResumeId());
        return toAjax(resumeEducationService.insertResumeEducation(resumeEducation));
    }

    /**
     * 修改教育背景
     */
    @Log(title = "教育背景", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ResumeEducation resumeEducation)
    {
        return toAjax(resumeEducationService.updateResumeEducation(resumeEducation));
    }

    /**
     * 删除教育背景
     */
    @Log(title = "教育背景", businessType = BusinessType.DELETE)
	@DeleteMapping("/{educationIds}")
    public AjaxResult remove(@PathVariable Long[] educationIds)
    {
        return toAjax(resumeEducationService.deleteResumeEducationByEducationIds(educationIds));
    }
}
