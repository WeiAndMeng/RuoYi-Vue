package com.ruoyi.web.controller.business.job;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.domain.Jobs;
import com.ruoyi.system.service.IJobsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * 职位发布Controller
 * 
 * @author ruoyi
 * @date 2025-01-15
 */
@RestController
@RequestMapping("/customer/jobs")
public class CustomerJobsController extends BaseController
{
    @Autowired
    private IJobsService jobsService;

    /**
     * 查询职位发布列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Jobs jobs)
    {
        startPage();
        List<Jobs> list = jobsService.selectCustomerJobsList(jobs);
        return getDataTable(list);
    }

    @GetMapping("/info/{jobId}")
    public AjaxResult info(@PathVariable Long jobId)
    {
        return success(jobsService.selectJobsByJobId(jobId));
    }

}
