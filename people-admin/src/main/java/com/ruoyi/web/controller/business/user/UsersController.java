package com.ruoyi.web.controller.business.user;

import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.service.ISysUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Users;
import com.ruoyi.system.service.IUsersService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 存储用户的基本信息Controller
 * 
 * @author ruoyi
 * @date 2025-01-13
 */
@RestController
@RequestMapping("/customer/users")
public class UsersController extends BaseController
{
    @Autowired
    private IUsersService usersService;

    @Autowired
    private ISysUserService sysUserService;

    /**
     * 查询存储用户的基本信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Users users)
    {
        startPage();
        List<Users> list = usersService.selectUsersList(users);
        return getDataTable(list);
    }

    /**
     * 导出存储用户的基本信息列表
     */
    @Log(title = "存储用户的基本信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Users users)
    {
        List<Users> list = usersService.selectUsersList(users);
        ExcelUtil<Users> util = new ExcelUtil<Users>(Users.class);
        util.exportExcel(response, list, "存储用户的基本信息数据");
    }

    /**
     * 获取存储用户的基本信息详细信息
     */
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") Long userId)
    {
        return success(usersService.selectUsersByUserId(userId));
    }

    /**
     * 获取存储用户的基本信息详细信息
     */
    @GetMapping(value = "/getByLoginUserId")
    public AjaxResult getByLoginUserId()
    {
        return success(usersService.getByLoginUserId(SecurityUtils.getUserId()));
    }

    /**
     * 新增存储用户的基本信息
     */
    @Log(title = "存储用户的基本信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Users users)
    {
        users.setSysUserId(SecurityUtils.getUserId());
        String msg = this.updatePassword(users);
        if (StringUtils.isNoneBlank(msg)) {
            return error(msg);
        }
        return toAjax(usersService.insertUsers(users));
    }

    /**
     * 修改存储用户的基本信息
     */
    @Log(title = "存储用户的基本信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Users users)
    {
        users.setSysUserId(SecurityUtils.getUserId());
        String msg = this.updatePassword(users);
        if (StringUtils.isNoneBlank(msg)) {
            return error(msg);
        }
        return toAjax(usersService.updateUsers(users));
    }

    private String updatePassword(Users users) {
        String msg = null;
        if (StringUtils.isNotBlank(users.getPassword())) {
            String password = users.getPassword();
            if (!Objects.equals(users.getPassword(), users.getConfirmPassword())) {
                msg = "请确认密码!";
            }else if (password.length() < UserConstants.PASSWORD_MIN_LENGTH
                    || password.length() > UserConstants.PASSWORD_MAX_LENGTH) {
                msg = "密码长度必须在5到20个字符之间!";
            }
            SysUser sysUser = sysUserService.selectUserById(users.getSysUserId());
            sysUser.setPassword(SecurityUtils.encryptPassword(password));
            sysUserService.updateUser(sysUser);
        }
        return msg;
    }

    /**
     * 删除存储用户的基本信息
     */
    @Log(title = "存储用户的基本信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable Long[] userIds)
    {
        return toAjax(usersService.deleteUsersByUserIds(userIds));
    }
}
