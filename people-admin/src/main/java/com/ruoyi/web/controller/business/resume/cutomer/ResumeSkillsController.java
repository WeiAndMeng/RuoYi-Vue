package com.ruoyi.web.controller.business.resume.cutomer;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.Resumes;
import com.ruoyi.system.service.IResumesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ResumeSkills;
import com.ruoyi.system.service.IResumeSkillsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 技能证书Controller
 * 
 * @author ruoyi
 * @date 2025-01-17
 */
@RestController
@RequestMapping("/resume/skills")
public class ResumeSkillsController extends BaseController
{
    @Autowired
    private IResumeSkillsService resumeSkillsService;

    @Autowired
    private IResumesService resumesService;

    /**
     * 查询技能证书列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ResumeSkills resumeSkills)
    {
        startPage();
        Resumes resumes = resumesService.getByLoginUserId(SecurityUtils.getUserId());
        if (resumes == null) {
            throw new ServiceException("请先维护简历信息！");
        }
        resumeSkills.setResumeId(resumes.getResumeId());
        List<ResumeSkills> list = resumeSkillsService.selectResumeSkillsList(resumeSkills);
        return getDataTable(list);
    }

    /**
     * 导出技能证书列表
     */
    @Log(title = "技能证书", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ResumeSkills resumeSkills)
    {
        List<ResumeSkills> list = resumeSkillsService.selectResumeSkillsList(resumeSkills);
        ExcelUtil<ResumeSkills> util = new ExcelUtil<ResumeSkills>(ResumeSkills.class);
        util.exportExcel(response, list, "技能证书数据");
    }

    /**
     * 获取技能证书详细信息
     */
    @GetMapping(value = "/{skillId}")
    public AjaxResult getInfo(@PathVariable("skillId") Long skillId)
    {
        return success(resumeSkillsService.selectResumeSkillsBySkillId(skillId));
    }

    /**
     * 新增技能证书
     */
    @Log(title = "技能证书", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ResumeSkills resumeSkills)
    {
        Resumes resumes = resumesService.getByLoginUserId(SecurityUtils.getUserId());
        if (resumes == null) {
            throw new ServiceException("请先维护简历信息！");
        }
        resumeSkills.setResumeId(resumes.getResumeId());
        return toAjax(resumeSkillsService.insertResumeSkills(resumeSkills));
    }

    /**
     * 修改技能证书
     */
    @Log(title = "技能证书", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ResumeSkills resumeSkills)
    {
        return toAjax(resumeSkillsService.updateResumeSkills(resumeSkills));
    }

    /**
     * 删除技能证书
     */
    @Log(title = "技能证书", businessType = BusinessType.DELETE)
	@DeleteMapping("/{skillIds}")
    public AjaxResult remove(@PathVariable Long[] skillIds)
    {
        return toAjax(resumeSkillsService.deleteResumeSkillsBySkillIds(skillIds));
    }
}
