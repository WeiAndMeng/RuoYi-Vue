package com.ruoyi.web.controller.business.course;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.Courses;
import com.ruoyi.system.service.ICoursesService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.CourseRegistrations;
import com.ruoyi.system.service.ICourseRegistrationsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 课程报名记录，记录用户报名信息及支付状态Controller
 * 
 * @author ruoyi
 * @date 2025-01-21
 */
@RestController
@RequestMapping("/course/registrations")
public class CourseRegistrationsController extends BaseController
{
    @Autowired
    private ICourseRegistrationsService courseRegistrationsService;

    @Autowired
    private ICoursesService coursesService;

    /**
     * 查询课程报名记录，记录用户报名信息及支付状态列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CourseRegistrations courseRegistrations)
    {
        startPage();
        Long userId = SecurityUtils.getUserId();
        if (SecurityUtils.isAdmin(userId)) {
            // 1.管理员看全部
        }else if (SecurityUtils.hasRole("expert")) {
            // 2.专家只能看到自己发布的课程
            List<Long> courseIds = coursesService.selectCoursesByPublishId(userId);
            if (CollectionUtils.isEmpty(courseIds)) {
                return getDataTable(new ArrayList<>());
            }
            courseRegistrations.setCourseIds(courseIds);
        }else if (SecurityUtils.hasRole("user")) {
            // 3.用户只能看到自己报名的课程
            courseRegistrations.setSysUserId(userId);
        }

        List<CourseRegistrations> list = courseRegistrationsService.selectCourseRegistrationsList(courseRegistrations);
        return getDataTable(list);
    }

    /**
     * 导出课程报名记录，记录用户报名信息及支付状态列表
     */
    @Log(title = "课程报名记录，记录用户报名信息及支付状态", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CourseRegistrations courseRegistrations)
    {
        List<CourseRegistrations> list = courseRegistrationsService.selectCourseRegistrationsList(courseRegistrations);
        ExcelUtil<CourseRegistrations> util = new ExcelUtil<CourseRegistrations>(CourseRegistrations.class);
        util.exportExcel(response, list, "课程报名记录，记录用户报名信息及支付状态数据");
    }

    /**
     * 获取课程报名记录，记录用户报名信息及支付状态详细信息
     */
    @GetMapping(value = "/{registrationId}")
    public AjaxResult getInfo(@PathVariable("registrationId") Long registrationId)
    {
        return success(courseRegistrationsService.selectCourseRegistrationsByRegistrationId(registrationId));
    }

    /**
     * 新增课程报名记录，记录用户报名信息及支付状态
     */
    @Log(title = "课程报名记录，记录用户报名信息及支付状态", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CourseRegistrations courseRegistrations)
    {
        return toAjax(courseRegistrationsService.insertCourseRegistrations(courseRegistrations));
    }

    /**
     * 修改课程报名记录，记录用户报名信息及支付状态
     */
    @Log(title = "课程报名记录，记录用户报名信息及支付状态", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CourseRegistrations courseRegistrations)
    {
        return toAjax(courseRegistrationsService.updateCourseRegistrations(courseRegistrations));
    }

    /**
     * 删除课程报名记录，记录用户报名信息及支付状态
     */
    @Log(title = "课程报名记录，记录用户报名信息及支付状态", businessType = BusinessType.DELETE)
	@DeleteMapping("/{registrationIds}")
    public AjaxResult remove(@PathVariable Long[] registrationIds)
    {
        return toAjax(courseRegistrationsService.deleteCourseRegistrationsByRegistrationIds(registrationIds));
    }

    /**
     * 报名课程
     */
    @Log(title = "课程报名记录，记录用户报名信息及支付状态", businessType = BusinessType.INSERT)
    @PostMapping("/addUserCourse")
    public AjaxResult addUserCourse(@RequestBody Courses courses)
    {
        Long userId = SecurityUtils.getUserId();
        return toAjax(courseRegistrationsService.addUserCourse(courses, userId));
    }

    /**
     * 删除课程报名记录，记录用户报名信息及支付状态
     */
    @Log(title = "课程报名记录，记录用户报名信息及支付状态", businessType = BusinessType.DELETE)
    @GetMapping("/payment/{registrationId}")
    public AjaxResult payment(@PathVariable Long registrationId)
    {
        CourseRegistrations courseRegistrations = new CourseRegistrations();
        courseRegistrations.setRegistrationId(registrationId);
        courseRegistrations.setPaymentStatus(1L);
        courseRegistrations.setUpdatedAt(new Date());
        return toAjax(courseRegistrationsService.updateCourseRegistrations(courseRegistrations));
    }
}
