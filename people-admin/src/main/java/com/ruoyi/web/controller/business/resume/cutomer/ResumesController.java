package com.ruoyi.web.controller.business.resume.cutomer;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.service.IResumeEducationService;
import com.ruoyi.system.service.IResumeExperiencesService;
import com.ruoyi.system.service.IResumeSkillsService;
import com.ruoyi.web.util.FileUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Resumes;
import com.ruoyi.system.service.IResumesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 简历Controller
 * 
 * @author ruoyi
 * @date 2025-01-15
 */
@RestController
@RequestMapping("/resume/info/")
public class ResumesController extends BaseController
{
    @Autowired
    private IResumesService resumesService;

    @Autowired
    private IResumeExperiencesService resumeExperiencesService;

    @Autowired
    private IResumeEducationService resumeEducationService;

    @Autowired
    private IResumeSkillsService resumeSkillsService;

    /**
     * 查询简历列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Resumes resumes)
    {
        startPage();
        if (!SecurityUtils.isAdmin(SecurityUtils.getUserId()) || !SecurityUtils.hasRole("manager")) {
            resumes.setSysUserId(SecurityUtils.getUserId());
        }
        List<Resumes> list = resumesService.selectResumesList(resumes);
        return getDataTable(list);
    }

    /**
     * 导出简历列表
     */
    @Log(title = "简历", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Resumes resumes)
    {
        List<Resumes> list = resumesService.selectResumesList(resumes);
        ExcelUtil<Resumes> util = new ExcelUtil<Resumes>(Resumes.class);
        util.exportExcel(response, list, "简历数据");
    }

    /**
     * 获取简历详细信息
     */
    @GetMapping(value = "/{resumeId}")
    public AjaxResult getInfo(@PathVariable("resumeId") Long resumeId)
    {
        return success(resumesService.selectResumesByResumeId(resumeId));
    }

    /**
     * 新增简历
     */
    @Log(title = "简历", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Resumes resumes)
    {
        Resumes resumes1 = resumesService.getByLoginUserId(SecurityUtils.getUserId());
        if (resumes1 != null) {
            return error("只允许存在一条简历信息！");
        }
        resumes.setSysUserId(SecurityUtils.getUserId());
        return toAjax(resumesService.insertResumes(resumes));
    }

    /**
     * 修改简历
     */
    @Log(title = "简历", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Resumes resumes)
    {
        return toAjax(resumesService.updateResumes(resumes));
    }

    /**
     * 删除简历
     */
    @Log(title = "简历", businessType = BusinessType.DELETE)
	@DeleteMapping("/{resumeIds}")
    @Transactional(rollbackFor = Exception.class)
    public AjaxResult remove(@PathVariable Long[] resumeIds)
    {
        for (int i = 0; i < resumeIds.length; i++) {
            Resumes resumes = resumesService.selectResumesByResumeId(resumeIds[i]);
            if (resumes != null && StringUtils.isNotBlank(resumes.getPreviewUrl())) {
                // 获取预览 URL
                String previewUrl = resumes.getPreviewUrl();
                FileUtil.deletePreviewFile(previewUrl);
            }
            // 删除工作经历
            resumeExperiencesService.deleteByResumeId(resumeIds[i]);

            // 删除教育背景
            resumeEducationService.deleteByResumeId(resumeIds[i]);

            // 删除技能证书
            resumeSkillsService.deleteByResumeId(resumeIds[i]);
        }
        return toAjax(resumesService.deleteResumesByResumeIds(resumeIds));
    }


    /**
     * 发布简历
     */
    @Log(title = "简历", businessType = BusinessType.UPDATE)
    @GetMapping("/push/{resumeId}")
    public AjaxResult pushResume(@PathVariable Long resumeId)
    {
        Resumes resumes = resumesService.selectResumesByResumeId(resumeId);
        if (resumes == null) {
            return error("简历不存在！");
        }
        if ("ACTIVE".equals(resumes.getStatus())) {
            return error("请勿重复发布！");
        }
        if (resumes.getTemplateId() == null && StringUtils.isBlank(resumes.getPreviewUrl())) {
            return error("请至简历生成菜单生成简历再发布！");
        }
        resumes.setStatus("ACTIVE");
        return toAjax(resumesService.updateResumes(resumes));
    }

}
