package com.ruoyi.web.controller.business.resume.template;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import com.ruoyi.web.util.FileUtil;
import freemarker.template.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 简历模板Controller
 *
 * @author ruoyi
 * @date 2025-01-15
 */
@RestController
@RequestMapping("/resume/templates")
public class ResumeTemplatesController extends BaseController
{
    @Autowired
    private IResumeTemplatesService resumeTemplatesService;

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private IResumesService resumesService;

    @Autowired
    private IResumeExperiencesService resumeExperiencesService;

    @Autowired
    private IResumeEducationService resumeEducationService;

    @Autowired
    private IResumeSkillsService resumeSkillsService;

    /**
     * 查询简历模板列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ResumeTemplates resumeTemplates)
    {
        startPage();
        List<ResumeTemplates> list = resumeTemplatesService.selectResumeTemplatesList(resumeTemplates);
        return getDataTable(list);
    }

    /**
     * 获取简历模板详细信息
     */
    @GetMapping(value = "/{templateId}")
    public AjaxResult getInfo(@PathVariable("templateId") Long templateId)
    {
        return success(resumeTemplatesService.selectResumeTemplatesByTemplateId(templateId));
    }

    /**
     * 生成简历模版
     */
    @GetMapping(value = "/generate/{templateId}")
    public AjaxResult generate(@PathVariable("templateId") Long templateId)
    {
        // 1. 获取模板信息
        ResumeTemplates resumeTemplates = resumeTemplatesService.selectResumeTemplatesByTemplateId(templateId);
        if (resumeTemplates == null) {
            return error("简历模板不存在！");
        }

        String ftlName = resumeTemplates.getFtlName();

        // 2. 加载 FreeMarker 配置
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_31);
        try {
            // 设置 FreeMarker 模板路径
            cfg.setClassLoaderForTemplateLoading(getClass().getClassLoader(), "templates");

            // 获取 FreeMarker 模板
            Template template = cfg.getTemplate(ftlName);

            // 3. 准备模板数据
            Map<String, Object> data = this.processResumeDate(SecurityUtils.getUserId());

            // 4. 生成 HTML 文件并保存到本地文件夹
            // 直接使用 RuoYiConfig.getProfile() 作为基础路径
            String previewUrl = FileUtil.generateHtmlFileAndGetPreviewUrl(template, data, RuoYiConfig.getProfile());

            // 个人简历主表
            Resumes resumes = resumesService.getByLoginUserId(SecurityUtils.getUserId());
            resumes.setTemplateId(resumeTemplates.getTemplateId());
            resumes.setPreviewUrl(previewUrl);  // 存储相对路径
            resumesService.updateResumes(resumes);

        } catch (IOException | TemplateException e) {
            logger.error(e.getMessage(), e);
            return AjaxResult.error("模板生成失败！");
        }
        return AjaxResult.success();
    }



    private Map<String, Object> processResumeDate(Long sysUserId) {

        // 获取用户
        SysUser sysUser = sysUserService.selectUserById(sysUserId);
        if (sysUser == null || StringUtils.isBlank(sysUser.getEmail()) || StringUtils.isBlank(sysUser.getPhonenumber())) {
            throw new ServiceException("请至用户信息菜单维护个人信息!");
        }

        // 个人简历主表
        Resumes resumes = resumesService.getByLoginUserId(sysUserId);
        if (resumes == null) {
            throw new ServiceException("请至简历管理菜单维护简历信息!");
        }

        // 工作经历信息
        List<ResumeExperiences> resumeExperiences = resumeExperiencesService.getByResumesId(resumes.getResumeId());
        if (CollectionUtils.isEmpty(resumeExperiences)) {
            throw new ServiceException("请至简历管理菜单维护工作经历信息!");
        }

        // 教育背景
        List<ResumeEducation> resumeEducations = resumeEducationService.getByResumesId(resumes.getResumeId());
        if (CollectionUtils.isEmpty(resumeEducations)) {
            throw new ServiceException("请至简历管理菜单维护教育背景!");
        }

        // 技能证书
        List<ResumeSkills> resumeSkills = resumeSkillsService.getByResumesId(resumes.getResumeId());
        if (CollectionUtils.isEmpty(resumeSkills)) {
            throw new ServiceException("请至简历管理菜单维护技能证书!");
        }

        // 数据模型
        Map<String, Object> data = new HashMap<>();
        data.put("title", resumes.getTitle());
        data.put("name", resumes.getName());
        data.put("summary", resumes.getSummary());

        // 基本信息
        data.put("birthDate", resumes.getBirthDate());
        data.put("phone", sysUser.getPhonenumber());
        data.put("email", sysUser.getEmail());
        data.put("address", resumes.getLocation());

        // 工作经历
        List<Map<String, String>> experiences = new ArrayList<>();
        for (ResumeExperiences resumeExperience : resumeExperiences) {
            Map<String, String> experience = new HashMap<>();
            experience.put("position", resumeExperience.getPosition());
            experience.put("company", resumeExperience.getCompany());
            experience.put("startDate", DateUtils.dateTime(resumeExperience.getStartDate()));
            experience.put("endDate", DateUtils.dateTime(resumeExperience.getEndDate()));
            experience.put("description", resumeExperience.getDescription());
            experiences.add(experience);
        }
        data.put("experiences", experiences);

        // 教育背景
        List<Map<String, String>> educations = new ArrayList<>();
        for (ResumeEducation resumeEducation : resumeEducations) {
            Map<String, String> education = new HashMap<>();
            education.put("schoolName", resumeEducation.getSchoolName());
            education.put("major", resumeEducation.getMajor());
            education.put("startDate", DateUtils.dateTime(resumeEducation.getStartDate()));
            education.put("endDate", DateUtils.dateTime(resumeEducation.getEndDate()));
            educations.add(education);
        }
        data.put("educations", educations);

        // 技能证书
        List<Map<String, String>> skills = new ArrayList<>();
        for (ResumeSkills resumeSkill : resumeSkills) {
            Map<String, String> skill = new HashMap<>();
            skill.put("skillName", resumeSkill.getSkillName());
            skill.put("certificateName", resumeSkill.getCertificateName());
            skills.add(skill);
        }
        data.put("skills", skills);

        return data;
    }
}
