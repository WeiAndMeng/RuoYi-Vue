package com.ruoyi.web.controller.chat;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import org.springframework.web.socket.TextMessage;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class WebSocketHandler extends TextWebSocketHandler {

    // 用于存储每个 chatId 对应的 WebSocket 会话
    private final Map<String, WebSocketSession> chatSessions = new ConcurrentHashMap<>();

    // 连接建立时的操作
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        String chatId = session.getUri().getPath().split("/")[2];  // 获取 chatId
        System.out.println("Chat room: " + chatId + " connected.");

        // 将 WebSocket 会话与 chatId 关联
        chatSessions.put(chatId, session);
        session.getAttributes().put("chatId", chatId);  // 将 chatId 存入 session
    }

    // 消息接收时的操作
    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        String chatId = (String) session.getAttributes().get("chatId");
        System.out.println("Received message for chat room " + chatId + ": " + message.getPayload());

        // 创建一个包含消息的对象
        Map<String, String> response = new HashMap<>();
        response.put("sender", "Server");
        response.put("content", message.getPayload());
        response.put("time", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date()));

        // 将消息对象转换为 JSON 字符串
        String jsonResponse = new ObjectMapper().writeValueAsString(response);

        // 向客户端发送有效的 JSON 格式的消息
        session.sendMessage(new TextMessage(jsonResponse));
    }

    // 连接关闭时的操作
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        String chatId = (String) session.getAttributes().get("chatId");
        System.out.println("Chat room " + chatId + " disconnected.");

        // 移除该 chatId 对应的 WebSocket 会话
        chatSessions.remove(chatId);
    }
}
