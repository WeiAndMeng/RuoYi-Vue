package com.ruoyi.web.controller.business.job;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.enums.JobStatus;
import com.ruoyi.common.enums.ResumeSubmissionStatus;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.Jobs;
import com.ruoyi.system.domain.ResumeSubmission;
import com.ruoyi.system.domain.vo.JobApprovalRequest;
import com.ruoyi.system.service.IJobsService;
import com.ruoyi.system.service.IResumeSubmissionService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 职位发布Controller
 * 
 * @author ruoyi
 * @date 2025-01-14
 */
@RestController
@RequestMapping("/manager/jobs")
public class JobsController extends BaseController
{
    @Autowired
    private IJobsService jobsService;

    @Autowired
    private IResumeSubmissionService resumeSubmissionService;

    /**
     * 查询职位发布列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Jobs jobs)
    {
        startPage();
        boolean role = SecurityUtils.isAdmin(SecurityUtils.getUserId());
        List<Jobs> list;
        if (!role) {
            // 雇主只可看到自己发布的信息
            Long userId = SecurityUtils.getUserId();
            jobs.setEmployerId(userId);
        }
        list = jobsService.selectJobsList(jobs);
        return getDataTable(list);
    }

    /**
     * 导出职位发布列表
     */
    @Log(title = "职位发布", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Jobs jobs)
    {
        List<Jobs> list = jobsService.selectJobsList(jobs);
        ExcelUtil<Jobs> util = new ExcelUtil<Jobs>(Jobs.class);
        util.exportExcel(response, list, "职位发布数据");
    }

    /**
     * 获取职位发布详细信息
     */
    @GetMapping(value = "/{jobId}")
    public AjaxResult getInfo(@PathVariable("jobId") Long jobId)
    {
        return success(jobsService.selectJobsByJobId(jobId));
    }

    /**
     * 新增职位发布
     */
    @Log(title = "职位发布", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Jobs jobs)
    {
        jobs.setEmployerId(SecurityUtils.getUserId());
        return toAjax(jobsService.insertJobs(jobs));
    }

    /**
     * 修改职位发布
     */
    @Log(title = "职位发布", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Jobs jobs)
    {
        return toAjax(jobsService.updateJobs(jobs));
    }

    /**
     * 删除职位发布
     */
    @Log(title = "职位发布", businessType = BusinessType.DELETE)
	@DeleteMapping("/{jobIds}")
    public AjaxResult remove(@PathVariable Long[] jobIds)
    {
        return toAjax(jobsService.deleteJobsByJobIds(jobIds));
    }

    @Log(title = "职位审核", businessType = BusinessType.UPDATE)
    @PostMapping("/{jobId}/approval")
    public AjaxResult approveJob(@PathVariable Long jobId, @RequestBody JobApprovalRequest request)
    {
        String msg = jobsService.approveJob(jobId, request.getStatus(), request.getComment());
        if (StringUtils.isNotBlank(msg)) {
            return error(msg);
        }
        String message = JobStatus.APPROVED.name().equals(request.getStatus()) ? "职位审核通过！" : "职位审核拒绝！";
        return success(message);
    }

    /**
     * 更新职位的上下架状态
     */
    @Log(title = "职位上下架", businessType = BusinessType.UPDATE)
    @PostMapping("/{jobId}/visibility")
    public AjaxResult updateJobVisibility(@PathVariable Long jobId, @RequestBody Jobs jobs)
    {
        String msg = jobsService.updateJobVisibility(jobId, jobs.getVisible());
        if (StringUtils.isNotBlank(msg)) {
            return error(msg);
        }
        return success();
    }

    @Log(title = "面试审核", businessType = BusinessType.UPDATE)
    @PostMapping("/{jobId}/{resumeId}/orderJob")
    public AjaxResult orderJob(@PathVariable Long jobId, @PathVariable Long resumeId, @RequestBody JobApprovalRequest request)
    {
        Jobs jobs = jobsService.selectJobsByJobId(jobId);
        if (jobs == null) {
            return error("职位不存在！");
        }
        ResumeSubmission resumeSubmission = resumeSubmissionService.selectByResumeIdAndJobId(resumeId, jobId);
        if (resumeSubmission == null) {
            return error("简历信息不存在！");
        }
        if (ResumeSubmissionStatus.HIRED.name().equals(resumeSubmission.getStatus()) ||
            ResumeSubmissionStatus.REJECTED.name().equals(resumeSubmission.getStatus())
        ) {
            return error("已操作过数据禁止重复操作！");
        }
        jobsService.orderJob(jobId, resumeId, request);
        return success();
    }
}
