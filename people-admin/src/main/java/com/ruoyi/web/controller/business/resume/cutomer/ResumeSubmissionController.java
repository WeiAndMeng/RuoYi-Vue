package com.ruoyi.web.controller.business.resume.cutomer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.enums.JobStatus;
import com.ruoyi.common.enums.ResumeSubmissionStatus;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.Jobs;
import com.ruoyi.system.domain.Resumes;
import com.ruoyi.system.domain.Users;
import com.ruoyi.system.service.IJobsService;
import com.ruoyi.system.service.IResumesService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ResumeSubmission;
import com.ruoyi.system.service.IResumeSubmissionService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 简历投递记录Controller
 * 
 * @author ruoyi
 * @date 2025-01-18
 */
@RestController
@RequestMapping("/resume/submission")
public class ResumeSubmissionController extends BaseController
{
    @Autowired
    private IResumeSubmissionService resumeSubmissionService;

    @Autowired
    private IResumesService resumesService;

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private IJobsService jobsService;

    /**
     * 查询简历投递记录列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ResumeSubmission resumeSubmission)
    {
        startPage();
        Long userId = SecurityUtils.getUserId();
        Resumes resumes = resumesService.getByLoginUserId(userId);
        if (resumes == null) {
            return getDataTable(new ArrayList<>());
        }
        resumeSubmission.setResumeId(resumes.getResumeId());
        List<ResumeSubmission> list = resumeSubmissionService.selectResumeSubmissionList(resumeSubmission);
        return getDataTable(list);
    }

    /**
     * 根据简历id查询简历投递记录列表
     */
    @GetMapping("/listByJobId")
    public TableDataInfo listByJobId(ResumeSubmission resumeSubmission)
    {
        startPage();
        Long userId = SecurityUtils.getUserId();
        Jobs jobs = jobsService.selectJobsByJobId(resumeSubmission.getJobId());
        if (jobs == null || !Objects.equals(jobs.getEmployerId(), userId)) {
            return getDataTable(new ArrayList<>());
        }
        resumeSubmission.setJobId(jobs.getJobId());
        List<ResumeSubmission> list = resumeSubmissionService.selectResumeSubmissionList(resumeSubmission);
        return getDataTable(list);
    }

    /**
     * 导出简历投递记录列表
     */
    @Log(title = "简历投递记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ResumeSubmission resumeSubmission)
    {
        List<ResumeSubmission> list = resumeSubmissionService.selectResumeSubmissionList(resumeSubmission);
        ExcelUtil<ResumeSubmission> util = new ExcelUtil<ResumeSubmission>(ResumeSubmission.class);
        util.exportExcel(response, list, "简历投递记录数据");
    }

    /**
     * 获取简历投递记录详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(resumeSubmissionService.selectResumeSubmissionById(id));
    }

    /**
     * 新增简历投递记录
     */
    @Log(title = "简历投递记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ResumeSubmission resumeSubmission)
    {
        Long userId = SecurityUtils.getUserId();
        Long jobId = resumeSubmission.getJobId();

        Resumes resumes = resumesService.getByLoginUserId(userId);
        if (resumes == null) {
            return error("请先维护简历信息");
        }
        if (resumes.getTemplateId() == null && StringUtils.isBlank(resumes.getPreviewUrl())) {
            return error("请至简历生成菜单生成简历");
        }
        if (!"ACTIVE".equals(resumes.getStatus())) {
            return error("未发布简历禁止投递");
        }
        // 获取用户
        SysUser sysUser = sysUserService.selectUserById(userId);
        if (sysUser == null || StringUtils.isBlank(sysUser.getEmail()) || StringUtils.isBlank(sysUser.getPhonenumber())) {
            throw new ServiceException("请至用户信息菜单维护个人信息!");
        }
        Jobs jobs = jobsService.selectJobsByJobId(jobId);
        if (jobs == null
                || !JobStatus.APPROVED.name().equals(jobs.getStatus())
                || 1 != jobs.getVisible()
        ) {
            throw new ServiceException("职位不存在或已关闭！");
        }

        // 校验是否已经投递
        ResumeSubmission oldData = resumeSubmissionService.selectByResumeIdAndJobId(resumes.getResumeId(), jobs.getJobId());
        if (oldData != null) {
            throw new ServiceException("请勿重复投递！");
        }

        resumeSubmission.setCandidateName(resumes.getName());
        resumeSubmission.setCandidateEmail(sysUser.getEmail());
        resumeSubmission.setCandidatePhone(sysUser.getPhonenumber());
        resumeSubmission.setResumeId(resumes.getResumeId());
        resumeSubmission.setResumeLink(resumes.getPreviewUrl());
        resumeSubmission.setJobId(jobs.getJobId());
        resumeSubmission.setJobTitle(jobs.getTitle());
        resumeSubmission.setStatus(ResumeSubmissionStatus.PENDING.name());
        resumeSubmission.setSubmissionDate(new Date());

        return toAjax(resumeSubmissionService.insertResumeSubmission(resumeSubmission));
    }

    /**
     * 修改简历投递记录
     */
    @Log(title = "简历投递记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ResumeSubmission resumeSubmission)
    {
        return toAjax(resumeSubmissionService.updateResumeSubmission(resumeSubmission));
    }

    /**
     * 删除简历投递记录
     */
    @Log(title = "简历投递记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(resumeSubmissionService.deleteResumeSubmissionByIds(ids));
    }
}
