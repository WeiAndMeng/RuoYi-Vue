package com.ruoyi.web.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.SecurityUtils;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriComponentsBuilder;

public class FileUtil {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public static String generateHtmlFileAndGetPreviewUrl(Template template, Map<String, Object> data, String baseDir) throws IOException, TemplateException {
        // 1. 生成唯一的文件名
        String fileName = generateUniqueFileName();

        // 2. 确保文件夹存在
        File outputDir = new File(baseDir);
        if (!outputDir.exists()) {
            outputDir.mkdirs();
        }

        // 3. 生成文件路径
        String outputPath = baseDir + File.separator + fileName + ".html";

        // 4. 检查文件是否存在，如果存在则删除
        File outputFile = new File(outputPath);
        if (outputFile.exists()) {
            outputFile.delete();
        }

        // 5. 生成 HTML 文件
        try (FileWriter writer = new FileWriter(outputPath)) {
            template.process(data, writer);
        }

        // 6. 返回相对路径或预览 URL
        return "/uploads/" + fileName + ".html"; // 这里假设您要返回的 URL 是相对于应用的路径
    }


    /**
     * 删除简历的预览文件
     */
    public static boolean deletePreviewFile(String previewUrl) {
        // 提取文件名
        String fileName = extractFileName(previewUrl);

        if (fileName == null) {
            throw new ServiceException("文件名提取失败，URL: " + previewUrl);
        }

        // 构建文件路径
        String filePath = buildFilePath(fileName);
        File file = new File(filePath);

        // 删除文件
        if (file.exists()) {
            boolean deleted = file.delete();
            if (!deleted) {
                throw new ServiceException("删除文件失败，URL: " + filePath);
            }
        } else {
            throw new ServiceException("文件不存在，URL: " + filePath);
        }

        return true;
    }

    /**
     * 从预览 URL 提取文件名
     */
    private static String extractFileName(String previewUrl) {
        if (StringUtils.isBlank(previewUrl)) {
            return null;
        }

        String fileName = previewUrl.substring(previewUrl.lastIndexOf("/") + 1);
        return fileName.isEmpty() ? null : fileName;
    }

    /**
     * 构建文件路径
     */
    private static String buildFilePath(String fileName) {
        // 直接使用 RuoYiConfig.getProfile() 获取文件的根目录
        String baseDir = RuoYiConfig.getProfile();
        return baseDir + File.separator + fileName;
    }

    private static String generateUniqueFileName() {
        return SecurityUtils.getUserId() + "";
    }

    /**
     * 构造预览 URL
     * @param fileName 文件名
     * @return 预览 URL
     */
    private static String constructPreviewUrl(String fileName) {
        String baseUrl = "http://localhost:8080";  // 获取配置文件中定义的 Web 地址
        return UriComponentsBuilder.fromHttpUrl(baseUrl)
                .path("/customerFtl/" + fileName + ".html")
                .toUriString();  // 动态构造 URL
    }
}
