import request from '@/utils/request'

// 查询教育背景列表
export function listEducation(query) {
  return request({
    url: '/resume/education/list',
    method: 'get',
    params: query
  })
}

// 查询教育背景详细
export function getEducation(educationId) {
  return request({
    url: '/resume/education/' + educationId,
    method: 'get'
  })
}

// 新增教育背景
export function addEducation(data) {
  return request({
    url: '/resume/education',
    method: 'post',
    data: data
  })
}

// 修改教育背景
export function updateEducation(data) {
  return request({
    url: '/resume/education',
    method: 'put',
    data: data
  })
}

// 删除教育背景
export function delEducation(educationId) {
  return request({
    url: '/resume/education/' + educationId,
    method: 'delete'
  })
}
