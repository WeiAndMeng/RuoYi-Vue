import request from '@/utils/request'

// 查询简历投递记录列表
export function listSubmission(query) {
  return request({
    url: '/resume/submission/list',
    method: 'get',
    params: query
  })
}

// 查询简历投递记录列表
export function listSubmissionByJobId(query) {
  return request({
    url: '/resume/submission/listByJobId',
    method: 'get',
    params: query
  })
}

// 查询简历投递记录详细
export function getSubmission(id) {
  return request({
    url: '/resume/submission/' + id,
    method: 'get'
  })
}

// 新增简历投递记录
export function addSubmission(data) {
  return request({
    url: '/resume/submission',
    method: 'post',
    data: data
  })
}

// 修改简历投递记录
export function updateSubmission(data) {
  return request({
    url: '/resume/submission',
    method: 'put',
    data: data
  })
}

// 删除简历投递记录
export function delSubmission(id) {
  return request({
    url: '/resume/submission/' + id,
    method: 'delete'
  })
}
