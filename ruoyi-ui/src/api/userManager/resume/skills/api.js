import request from '@/utils/request'

// 查询技能证书列表
export function listSkills(query) {
  return request({
    url: '/resume/skills/list',
    method: 'get',
    params: query
  })
}

// 查询技能证书详细
export function getSkills(skillId) {
  return request({
    url: '/resume/skills/' + skillId,
    method: 'get'
  })
}

// 新增技能证书
export function addSkills(data) {
  return request({
    url: '/resume/skills',
    method: 'post',
    data: data
  })
}

// 修改技能证书
export function updateSkills(data) {
  return request({
    url: '/resume/skills',
    method: 'put',
    data: data
  })
}

// 删除技能证书
export function delSkills(skillId) {
  return request({
    url: '/resume/skills/' + skillId,
    method: 'delete'
  })
}
