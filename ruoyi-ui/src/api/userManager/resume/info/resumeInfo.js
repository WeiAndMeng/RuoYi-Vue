import request from '@/utils/request'

// 查询简历列表
export function listResumes(query) {
  return request({
    url: '/resume/info/list',
    method: 'get',
    params: query
  })
}

// 查询简历详细
export function getResumes(resumeId) {
  return request({
    url: '/resume/info/' + resumeId,
    method: 'get'
  })
}

// 新增简历
export function addResumes(data) {
  return request({
    url: '/resume/info/',
    method: 'post',
    data: data
  })
}

// 修改简历
export function updateResumes(data) {
  return request({
    url: '/resume/info/',
    method: 'put',
    data: data
  })
}

// 删除简历
export function delResumes(resumeId) {
  return request({
    url: '/resume/info/' + resumeId,
    method: 'delete'
  })
}

// 发布简历
export function pushResumes(resumeId) {
  return request({
    url: '/resume/info/push/' + resumeId,
    method: 'get'
  })
}

// 查询简历模板列表
export function downloadPDF(data) {
  return request({
    url: '/resume/info/generatePdf',
    method: 'post',
    data: data,
    responseType: 'arraybuffer' // 重要，确保返回的是 PDF 文件
  })
}
