import request from '@/utils/request'

// 查询工作经历列表
export function listExperiences(query) {
  return request({
    url: '/resume/experiences/list',
    method: 'get',
    params: query
  })
}

// 查询工作经历详细
export function getExperiences(experienceId) {
  return request({
    url: '/resume/experiences/' + experienceId,
    method: 'get'
  })
}

// 新增工作经历
export function addExperiences(data) {
  return request({
    url: '/resume/experiences',
    method: 'post',
    data: data
  })
}

// 修改工作经历
export function updateExperiences(data) {
  return request({
    url: '/resume/experiences',
    method: 'put',
    data: data
  })
}

// 删除工作经历
export function delExperiences(experienceId) {
  return request({
    url: '/resume/experiences/' + experienceId,
    method: 'delete'
  })
}
