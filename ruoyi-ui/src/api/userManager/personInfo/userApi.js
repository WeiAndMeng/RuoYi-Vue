import request from '@/utils/request'

// 查询存储用户的基本信息列表
export function listUsers(query) {
  return request({
    url: '/customer/users/list',
    method: 'get',
    params: query
  })
}

// 查询存储用户的基本信息详细
export function getUsers(userId) {
  return request({
    url: '/customer/users/' + userId,
    method: 'get'
  })
}

// 新增存储用户的基本信息
export function addUsers(data) {
  return request({
    url: '/customer/users',
    method: 'post',
    data: data
  })
}

// 修改存储用户的基本信息
export function updateUsers(data) {
  return request({
    url: '/customer/users',
    method: 'put',
    data: data
  })
}

// 删除存储用户的基本信息
export function delUsers(userId) {
  return request({
    url: '/customer/users/' + userId,
    method: 'delete'
  })
}

// 根据登录用户查询存储用户的基本信息详细
export function getByLoginUserId() {
  return request({
    url: '/customer/users/getByLoginUserId',
    method: 'get'
  })
}
