import request from '@/utils/request'

// 查询简历模板列表
export function listTemplates(query) {
  return request({
    url: '/resume/templates/list',
    method: 'get',
    params: query
  })
}

// 查询简历模板详细
export function getTemplates(templateId) {
  return request({
    url: '/resume/templates/' + templateId,
    method: 'get'
  })
}

export function generateTemplate(templateId){
  return request({
    url: '/resume/templates/generate/' + templateId,
    method: 'get'
  })
}
