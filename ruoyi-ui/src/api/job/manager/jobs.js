import request from '@/utils/request'

// 查询职位发布列表
export function listJobs(query) {
  return request({
    url: '/manager/jobs/list',
    method: 'get',
    params: query
  })
}

// 查询职位发布详细
export function getJobs(jobId) {
  return request({
    url: '/manager/jobs/' + jobId,
    method: 'get'
  })
}

// 新增职位发布
export function addJobs(data) {
  return request({
    url: '/manager/jobs',
    method: 'post',
    data: data
  })
}

// 修改职位发布
export function updateJobs(data) {
  return request({
    url: '/manager/jobs',
    method: 'put',
    data: data
  })
}

// 删除职位发布
export function delJobs(jobId) {
  return request({
    url: '/manager/jobs/' + jobId,
    method: 'delete'
  })
}

// 提交审核结果
export function approveJob(jobId, status, comment) {
  return request({
    url: `/manager/jobs/${jobId}/approval`,
    method: 'post',
    data: { status, comment },
  });
}

// 上下架
export function updateJobVisibility(jobId, visible) {
  return request({
    url: `/manager/jobs/${jobId}/visibility`,
    method: 'post',
    data: {
      visible
    }
  });
}

// 面试审核
export function orderJob(jobId, resumeId, status, comment, orderDate) {
  return request({
    url: `/manager/jobs/${jobId}/${resumeId}/orderJob`,
    method: 'post',
    data: { status, comment, orderDate},
  });
}
