import request from '@/utils/request'

// 查询职位发布列表
export function listJobs(query) {
  return request({
    url: '/customer/jobs/list',
    method: 'get',
    params: query
  })
}

// 详情页
export function jobInfo(id) {
  return request({
    url: '/customer/jobs/info/' + id,
    method: 'get',
  })
}
