import request from '@/utils/request'

// 查询课程报名记录，记录用户报名信息及支付状态列表
export function listRegistrations(query) {
  return request({
    url: '/course/registrations/list',
    method: 'get',
    params: query
  })
}

// 查询课程报名记录，记录用户报名信息及支付状态详细
export function getRegistrations(registrationId) {
  return request({
    url: '/course/registrations/' + registrationId,
    method: 'get'
  })
}

// 新增课程报名记录，记录用户报名信息及支付状态
export function addRegistrations(data) {
  return request({
    url: '/course/registrations',
    method: 'post',
    data: data
  })
}

// 修改课程报名记录，记录用户报名信息及支付状态
export function updateRegistrations(data) {
  return request({
    url: '/course/registrations',
    method: 'put',
    data: data
  })
}

// 删除课程报名记录，记录用户报名信息及支付状态
export function delRegistrations(registrationId) {
  return request({
    url: '/course/registrations/' + registrationId,
    method: 'delete'
  })
}

// 报名
export function addUserCourse(data) {
  return request({
    url: '/course/registrations/addUserCourse',
    method: 'post',
    data: data
  })
}

// 删除课程报名记录，记录用户报名信息及支付状态
export function payment(registrationId) {
  return request({
    url: '/course/registrations/payment/' + registrationId,
    method: 'get'
  })
}
