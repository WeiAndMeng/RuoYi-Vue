package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 存储用户的基本信息对象 users
 */
public class Users extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户唯一标识（自增） */
    private Long userId;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 邮箱地址 */
    @Excel(name = "邮箱地址")
    private String email;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String phone;

    /** 年龄 */
    @Excel(name = "年龄")
    private Long age;

    /** 退休前职业 */
    @Excel(name = "退休前职业")
    private String oldJob;

    /** 技能 */
    @Excel(name = "技能")
    private String skills;

    /** 兴趣爱好 */
    @Excel(name = "兴趣爱好")
    private String hobbies;

    /** 关联登录用户ID */
    @Excel(name = "关联登录用户ID")
    private Long sysUserId;

    /** 注册时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "注册时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 最后更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    /**
     * 密码
     */
    private String password;

    /**
     * 确认密码
     */
    private String confirmPassword;

    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setAge(Long age) 
    {
        this.age = age;
    }

    public Long getAge() 
    {
        return age;
    }
    public void setOldJob(String oldJob) 
    {
        this.oldJob = oldJob;
    }

    public String getOldJob() 
    {
        return oldJob;
    }
    public void setSkills(String skills) 
    {
        this.skills = skills;
    }

    public String getSkills() 
    {
        return skills;
    }
    public void setHobbies(String hobbies) 
    {
        this.hobbies = hobbies;
    }

    public String getHobbies() 
    {
        return hobbies;
    }
    public void setSysUserId(Long sysUserId) 
    {
        this.sysUserId = sysUserId;
    }

    public Long getSysUserId() 
    {
        return sysUserId;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", getUserId())
            .append("username", getUsername())
            .append("email", getEmail())
            .append("phone", getPhone())
            .append("age", getAge())
            .append("oldJob", getOldJob())
            .append("skills", getSkills())
            .append("hobbies", getHobbies())
            .append("sysUserId", getSysUserId())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
