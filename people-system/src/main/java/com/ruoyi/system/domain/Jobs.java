package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.enums.JobStatus;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 职位发布对象 jobs
 * 
 * @author ruoyi
 * @date 2025-01-14
 */
public class Jobs extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 职位唯一标识（自增） */
    private Long jobId;

    /** 发布职位的用户ID（关联users表） */
    private Long employerId;

    /** 职位名称 */
    @Excel(name = "职位名称")
    private String title;

    /** 职位描述或工作内容 */
    @Excel(name = "职位描述")
    private String description;

    /** 技能要求（以逗号分隔） */
    @Excel(name = "技能要求", readConverterExp = "以=逗号分隔")
    private String skillsRequired;

    /** 最低薪资 */
    @Excel(name = "最低薪资")
    private BigDecimal salaryMin;

    /** 最高薪资 */
    @Excel(name = "最高薪资")
    private BigDecimal salaryMax;

    /** 工作地点 */
    @Excel(name = "工作地点")
    private String location;

    /** 职位类型ID（关联job_types表） */
    private Long jobTypeId;

    /** 职位状态 */
    private String status;

    /**
     * 审核状态
     */
    @Excel(name = "职位状态")
    private String statusName;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 最后更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    /**
     * 职位类型
     */
    @Excel(name = "职位状态")
    private String jobTypeDesc;

    /**
     * 上下架状态，1表示上架，0表示下架
     */
    private Integer visible;

    public void setJobId(Long jobId)
    {
        this.jobId = jobId;
    }

    public Long getJobId() 
    {
        return jobId;
    }
    public void setEmployerId(Long employerId) 
    {
        this.employerId = employerId;
    }

    public Long getEmployerId() 
    {
        return employerId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setSkillsRequired(String skillsRequired) 
    {
        this.skillsRequired = skillsRequired;
    }

    public String getSkillsRequired() 
    {
        return skillsRequired;
    }
    public void setSalaryMin(BigDecimal salaryMin) 
    {
        this.salaryMin = salaryMin;
    }

    public BigDecimal getSalaryMin() 
    {
        return salaryMin;
    }
    public void setSalaryMax(BigDecimal salaryMax) 
    {
        this.salaryMax = salaryMax;
    }

    public BigDecimal getSalaryMax() 
    {
        return salaryMax;
    }
    public void setLocation(String location) 
    {
        this.location = location;
    }

    public String getLocation() 
    {
        return location;
    }
    public void setJobTypeId(Long jobTypeId) 
    {
        this.jobTypeId = jobTypeId;
    }

    public Long getJobTypeId() 
    {
        return jobTypeId;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    public String getJobTypeDesc() {
        return jobTypeDesc;
    }

    public void setJobTypeDesc(String jobTypeDesc) {
        this.jobTypeDesc = jobTypeDesc;
    }

    public String getStatusName() {
        return JobStatus.fromValue(this.status) == null ? null : JobStatus.fromValue(this.status).getDescription();
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("jobId", getJobId())
            .append("employerId", getEmployerId())
            .append("title", getTitle())
            .append("description", getDescription())
            .append("skillsRequired", getSkillsRequired())
            .append("salaryMin", getSalaryMin())
            .append("salaryMax", getSalaryMax())
            .append("location", getLocation())
            .append("jobTypeId", getJobTypeId())
            .append("status", getStatus())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
