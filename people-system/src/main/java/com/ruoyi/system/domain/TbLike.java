package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 点赞对象 tb_like
 * 
 * @author ruoyi
 * @date 2024-03-30
 */
public class TbLike extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 用户编号 */
    @Excel(name = "用户编号")
    private String userId;

    /** 内容类型，1：中药大全 2：中药方剂 */
    @Excel(name = "内容类型，1：中药大全 2：中药方剂")
    private Long contentType;

    /** 内容id */
    @Excel(name = "内容id")
    private Long contentId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setContentType(Long contentType) 
    {
        this.contentType = contentType;
    }

    public Long getContentType() 
    {
        return contentType;
    }
    public void setContentId(Long contentId) 
    {
        this.contentId = contentId;
    }

    public Long getContentId() 
    {
        return contentId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("contentType", getContentType())
            .append("contentId", getContentId())
            .append("createTime", getCreateTime())
            .toString();
    }
}
