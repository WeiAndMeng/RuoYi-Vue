package com.ruoyi.system.domain.vo;

public class FilePdfVO {

    private String urlParams;

    public String getUrlParams() {
        return urlParams;
    }

    public void setUrlParams(String urlParams) {
        this.urlParams = urlParams;
    }
}
