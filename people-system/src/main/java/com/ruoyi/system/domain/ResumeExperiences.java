package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 工作经历对象 resume_experiences
 * 
 * @author ruoyi
 * @date 2025-01-15
 */
public class ResumeExperiences extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 工作经历唯一标识 */
    private Long experienceId;

    /** 关联的简历ID（关联resumes表） */
    @Excel(name = "关联的简历ID", readConverterExp = "关=联resumes表")
    private Long resumeId;

    /** 职位名称 */
    @Excel(name = "职位名称")
    private String position;

    /** 公司名称 */
    @Excel(name = "公司名称")
    private String company;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /** 结束时间（为空则表示目前仍在职） */
    @Excel(name = "结束时间", readConverterExp = "为=空则表示目前仍在职")
    private Date endDate;

    /** 工作内容描述 */
    @Excel(name = "工作内容描述")
    private String description;

    public void setExperienceId(Long experienceId) 
    {
        this.experienceId = experienceId;
    }

    public Long getExperienceId() 
    {
        return experienceId;
    }
    public void setResumeId(Long resumeId) 
    {
        this.resumeId = resumeId;
    }

    public Long getResumeId() 
    {
        return resumeId;
    }
    public void setPosition(String position) 
    {
        this.position = position;
    }

    public String getPosition() 
    {
        return position;
    }
    public void setCompany(String company) 
    {
        this.company = company;
    }

    public String getCompany() 
    {
        return company;
    }
    public void setStartDate(Date startDate) 
    {
        this.startDate = startDate;
    }

    public Date getStartDate() 
    {
        return startDate;
    }
    public void setEndDate(Date endDate) 
    {
        this.endDate = endDate;
    }

    public Date getEndDate() 
    {
        return endDate;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("experienceId", getExperienceId())
            .append("resumeId", getResumeId())
            .append("position", getPosition())
            .append("company", getCompany())
            .append("startDate", getStartDate())
            .append("endDate", getEndDate())
            .append("description", getDescription())
            .toString();
    }
}
