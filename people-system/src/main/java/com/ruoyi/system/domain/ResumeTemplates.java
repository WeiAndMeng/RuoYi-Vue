package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 简历模板对象 resume_templates
 *
 * @author ruoyi
 * @date 2025-01-15
 */
public class ResumeTemplates extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 简历模板唯一标识 */
    private Long templateId;

    /** 模板名称 */
    @Excel(name = "模板名称")
    private String name;

    /** 模板描述 */
    @Excel(name = "模板描述")
    private String description;

    /** 模板预览URL */
    @Excel(name = "模板预览URL")
    private String previewUrl;

    /** 模版全称 */
    @Excel(name = "模版全称")
    private String ftlName;

    public void setTemplateId(Long templateId)
    {
        this.templateId = templateId;
    }

    public Long getTemplateId()
    {
        return templateId;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }
    public void setPreviewUrl(String previewUrl)
    {
        this.previewUrl = previewUrl;
    }

    public String getPreviewUrl()
    {
        return previewUrl;
    }
    public void setFtlName(String ftlName)
    {
        this.ftlName = ftlName;
    }

    public String getFtlName()
    {
        return ftlName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("templateId", getTemplateId())
                .append("name", getName())
                .append("description", getDescription())
                .append("previewUrl", getPreviewUrl())
                .append("ftlName", getFtlName())
                .toString();
    }
}
