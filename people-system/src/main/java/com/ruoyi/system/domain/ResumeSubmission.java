package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.enums.ResumeSubmissionStatus;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 简历投递记录对象 resume_submission
 * 
 * @author ruoyi
 * @date 2025-01-18
 */
public class ResumeSubmission extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 投递记录ID */
    private Long id;

    /** 应聘者姓名 */
    @Excel(name = "应聘者姓名")
    private String candidateName;

    /** 应聘者邮箱 */
    @Excel(name = "应聘者邮箱")
    private String candidateEmail;

    /** 应聘者电话 */
    @Excel(name = "应聘者电话")
    private String candidatePhone;

    /** 投递者简历ID */
    private Long resumeId;

    /** 简历文件链接 */
    @Excel(name = "简历文件链接")
    private String resumeLink;

    /** 投递的职位ID */
    private Long jobId;

    /** 职位名称 */
    @Excel(name = "职位名称")
    private String jobTitle;

    /** 投递时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "投递时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date submissionDate;

    /** 面试时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "面试时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderDate;


    /** 投递状态: 待处理, 面试中, 已录用, 未录用 */
    @Excel(name = "投递状态: 待处理, 面试中, 已录用, 未录用")
    private String status;

    private String statusName;

    /** 面试或评估的备注 */
    @Excel(name = "面试或评估的备注")
    private String remarks;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCandidateName(String candidateName) 
    {
        this.candidateName = candidateName;
    }

    public String getCandidateName() 
    {
        return candidateName;
    }
    public void setCandidateEmail(String candidateEmail) 
    {
        this.candidateEmail = candidateEmail;
    }

    public String getCandidateEmail() 
    {
        return candidateEmail;
    }
    public void setCandidatePhone(String candidatePhone) 
    {
        this.candidatePhone = candidatePhone;
    }

    public String getCandidatePhone() 
    {
        return candidatePhone;
    }
    public void setResumeId(Long resumeId) 
    {
        this.resumeId = resumeId;
    }

    public Long getResumeId() 
    {
        return resumeId;
    }
    public void setResumeLink(String resumeLink) 
    {
        this.resumeLink = resumeLink;
    }

    public String getResumeLink() 
    {
        return resumeLink;
    }
    public void setJobId(Long jobId) 
    {
        this.jobId = jobId;
    }

    public Long getJobId() 
    {
        return jobId;
    }
    public void setJobTitle(String jobTitle) 
    {
        this.jobTitle = jobTitle;
    }

    public String getJobTitle() 
    {
        return jobTitle;
    }
    public void setSubmissionDate(Date submissionDate) 
    {
        this.submissionDate = submissionDate;
    }

    public Date getSubmissionDate() 
    {
        return submissionDate;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getStatusName() {
        return ResumeSubmissionStatus.fromLabel(this.status) == null ? null : ResumeSubmissionStatus.fromLabel(this.status).getLabel();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("candidateName", getCandidateName())
            .append("candidateEmail", getCandidateEmail())
            .append("candidatePhone", getCandidatePhone())
            .append("resumeId", getResumeId())
            .append("resumeLink", getResumeLink())
            .append("jobId", getJobId())
            .append("jobTitle", getJobTitle())
            .append("submissionDate", getSubmissionDate())
            .append("status", getStatus())
            .append("remarks", getRemarks())
            .toString();
    }
}
