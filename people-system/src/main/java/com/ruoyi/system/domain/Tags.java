package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 标签对象 tags
 * 
 * @author ruoyi
 * @date 2025-01-14
 */
public class Tags extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标签唯一标识（自增） */
    private Long tagId;

    /** 标签名称，例如 老年友好、灵活时间 */
    @Excel(name = "标签名称，例如 老年友好、灵活时间")
    private String name;

    /** 标签描述 */
    @Excel(name = "标签描述")
    private String description;

    public void setTagId(Long tagId) 
    {
        this.tagId = tagId;
    }

    public Long getTagId() 
    {
        return tagId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("tagId", getTagId())
            .append("name", getName())
            .append("description", getDescription())
            .toString();
    }
}
