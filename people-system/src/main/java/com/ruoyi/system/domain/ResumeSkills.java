package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 技能证书对象 resume_skills
 * 
 * @author ruoyi
 * @date 2025-01-15
 */
public class ResumeSkills extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 技能证书唯一标识 */
    private Long skillId;

    /** 关联的简历ID（关联resumes表） */
    @Excel(name = "关联的简历ID", readConverterExp = "关=联resumes表")
    private Long resumeId;

    /** 技能名称 */
    @Excel(name = "技能名称")
    private String skillName;

    /** 证书名称 */
    @Excel(name = "证书名称")
    private String certificateName;

    /** 发证机构 */
    @Excel(name = "发证机构")
    private String organization;

    /** 获得时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "获得时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date obtainedDate;

    public void setSkillId(Long skillId) 
    {
        this.skillId = skillId;
    }

    public Long getSkillId() 
    {
        return skillId;
    }
    public void setResumeId(Long resumeId) 
    {
        this.resumeId = resumeId;
    }

    public Long getResumeId() 
    {
        return resumeId;
    }
    public void setSkillName(String skillName) 
    {
        this.skillName = skillName;
    }

    public String getSkillName() 
    {
        return skillName;
    }
    public void setCertificateName(String certificateName) 
    {
        this.certificateName = certificateName;
    }

    public String getCertificateName() 
    {
        return certificateName;
    }
    public void setOrganization(String organization) 
    {
        this.organization = organization;
    }

    public String getOrganization() 
    {
        return organization;
    }
    public void setObtainedDate(Date obtainedDate) 
    {
        this.obtainedDate = obtainedDate;
    }

    public Date getObtainedDate() 
    {
        return obtainedDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("skillId", getSkillId())
            .append("resumeId", getResumeId())
            .append("skillName", getSkillName())
            .append("certificateName", getCertificateName())
            .append("organization", getOrganization())
            .append("obtainedDate", getObtainedDate())
            .toString();
    }
}
