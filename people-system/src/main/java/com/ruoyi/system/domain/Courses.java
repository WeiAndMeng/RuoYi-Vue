package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 课程，存储所有课程信息对象 courses
 * 
 * @author ruoyi
 * @date 2025-01-21
 */
public class Courses extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 课程ID */
    private Long courseId;

    /** 课程标题 */
    @Excel(name = "课程标题")
    private String title;

    /** 课程简介 */
    @Excel(name = "课程简介")
    private String description;

    /** 适合人群 */
    @Excel(name = "适合人群")
    private String targetGroup;

    /** 课程价格 */
    @Excel(name = "课程价格")
    private BigDecimal price;

    /** 课程时长（分钟） */
    @Excel(name = "课程时长", readConverterExp = "分=钟")
    private Long duration;

    /** 授课方式（线上/线下） */
    @Excel(name = "授课方式", readConverterExp = "线=上/线下")
    private String deliveryMode;

    /** 发布人ID（管理员或课程创建者的ID） */
    @Excel(name = "发布人ID", readConverterExp = "管=理员或课程创建者的ID")
    private Long publisherId;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setCourseId(Long courseId) 
    {
        this.courseId = courseId;
    }

    public Long getCourseId() 
    {
        return courseId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setTargetGroup(String targetGroup) 
    {
        this.targetGroup = targetGroup;
    }

    public String getTargetGroup() 
    {
        return targetGroup;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setDuration(Long duration) 
    {
        this.duration = duration;
    }

    public Long getDuration() 
    {
        return duration;
    }
    public void setDeliveryMode(String deliveryMode) 
    {
        this.deliveryMode = deliveryMode;
    }

    public String getDeliveryMode() 
    {
        return deliveryMode;
    }
    public void setPublisherId(Long publisherId) 
    {
        this.publisherId = publisherId;
    }

    public Long getPublisherId() 
    {
        return publisherId;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("courseId", getCourseId())
            .append("title", getTitle())
            .append("description", getDescription())
            .append("targetGroup", getTargetGroup())
            .append("price", getPrice())
            .append("duration", getDuration())
            .append("deliveryMode", getDeliveryMode())
            .append("publisherId", getPublisherId())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
