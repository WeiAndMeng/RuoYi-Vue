package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 教育背景对象 resume_education
 * 
 * @author ruoyi
 * @date 2025-01-15
 */
public class ResumeEducation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 教育背景唯一标识 */
    private Long educationId;

    /** 关联的简历ID（关联resumes表） */
    @Excel(name = "关联的简历ID", readConverterExp = "关=联resumes表")
    private Long resumeId;

    /** 学校名称 */
    @Excel(name = "学校名称")
    private String schoolName;

    /** 专业名称 */
    @Excel(name = "专业名称")
    private String major;

    /** 学历 */
    @Excel(name = "学历")
    private String degree;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    public void setEducationId(Long educationId) 
    {
        this.educationId = educationId;
    }

    public Long getEducationId() 
    {
        return educationId;
    }
    public void setResumeId(Long resumeId) 
    {
        this.resumeId = resumeId;
    }

    public Long getResumeId() 
    {
        return resumeId;
    }
    public void setSchoolName(String schoolName) 
    {
        this.schoolName = schoolName;
    }

    public String getSchoolName() 
    {
        return schoolName;
    }
    public void setMajor(String major) 
    {
        this.major = major;
    }

    public String getMajor() 
    {
        return major;
    }
    public void setDegree(String degree) 
    {
        this.degree = degree;
    }

    public String getDegree() 
    {
        return degree;
    }
    public void setStartDate(Date startDate) 
    {
        this.startDate = startDate;
    }

    public Date getStartDate() 
    {
        return startDate;
    }
    public void setEndDate(Date endDate) 
    {
        this.endDate = endDate;
    }

    public Date getEndDate() 
    {
        return endDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("educationId", getEducationId())
            .append("resumeId", getResumeId())
            .append("schoolName", getSchoolName())
            .append("major", getMajor())
            .append("degree", getDegree())
            .append("startDate", getStartDate())
            .append("endDate", getEndDate())
            .toString();
    }
}
