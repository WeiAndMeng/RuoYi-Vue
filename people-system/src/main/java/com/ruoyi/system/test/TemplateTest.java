package com.ruoyi.system.test;

import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TemplateTest {
    public static void main(String[] args) throws Exception {
        // 创建Freemarker配置对象
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_31);

        // 使用 ClassLoader 加载模板文件
        cfg.setClassLoaderForTemplateLoading(Thread.currentThread().getContextClassLoader(), "templates");
        cfg.setDefaultEncoding("UTF-8");

        // 获取模板
//        Template template = cfg.getTemplate("resume-template.ftl");
//        Template template = cfg.getTemplate("creative-template.ftl");
//        Template template = cfg.getTemplate("modern-template.ftl");
        Template template = cfg.getTemplate("simple-template.ftl");

        // 数据模型
        Map<String, Object> data = new HashMap<>();
        data.put("title", "我的简历");
        data.put("name", "张三");
        data.put("summary", "一名积极向上的开发工程师，具有扎实的计算机科学基础和丰富的项目经验。");

        // 基本信息
        data.put("birthDate", "1995-05-15");
        data.put("phone", "123-4567-8901");
        data.put("email", "zhangsan@example.com");
        data.put("address", "北京市朝阳区");

        // 工作经历
        List<Map<String, String>> experiences = new ArrayList<>();
        Map<String, String> experience1 = new HashMap<>();
        experience1.put("position", "软件工程师");
        experience1.put("company", "ABC公司");
        experience1.put("startDate", "2020-01");
        experience1.put("endDate", "2023-01");
        experience1.put("description", "负责系统开发与维护，参与多个企业级项目，优化系统性能，提升用户体验。");

        Map<String, String> experience2 = new HashMap<>();
        experience2.put("position", "初级开发工程师");
        experience2.put("company", "XYZ公司");
        experience2.put("startDate", "2018-06");
        experience2.put("endDate", "2019-12");
        experience2.put("description", "参与开发公司内部工具，进行前后端联调，提升开发效率。");

        experiences.add(experience1);
        experiences.add(experience2);
        data.put("experiences", experiences);

        // 教育背景
        List<Map<String, String>> educations = new ArrayList<>();
        Map<String, String> education = new HashMap<>();
        education.put("schoolName", "XX大学");
        education.put("major", "计算机科学");
        education.put("startDate", "2016-09");
        education.put("endDate", "2020-07");
        educations.add(education);
        data.put("educations", educations);

        // 技能证书
        List<Map<String, String>> skills = new ArrayList<>();
        Map<String, String> skill1 = new HashMap<>();
        skill1.put("skillName", "Java编程");
        skill1.put("certificateName", "Oracle认证");

        Map<String, String> skill2 = new HashMap<>();
        skill2.put("skillName", "前端开发");
        skill2.put("certificateName", "Vue.js开发认证");

        Map<String, String> skill3 = new HashMap<>();
        skill3.put("skillName", "数据库管理");
        skill3.put("certificateName", "MySQL认证");

        skills.add(skill1);
        skills.add(skill2);
        skills.add(skill3);
        data.put("skills", skills);

        // 输出到 HTML 文件
        FileWriter writer = new FileWriter("simple-template.html");
        template.process(data, writer);
        writer.close();

        System.out.println("简历已生成：output.html");
    }
}
