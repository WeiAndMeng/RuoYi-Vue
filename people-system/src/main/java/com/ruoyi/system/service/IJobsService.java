package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Jobs;
import com.ruoyi.system.domain.vo.JobApprovalRequest;

/**
 * 职位发布Service接口
 * 
 * @author ruoyi
 * @date 2025-01-14
 */
public interface IJobsService 
{
    /**
     * 查询职位发布
     * 
     * @param jobId 职位发布主键
     * @return 职位发布
     */
    public Jobs selectJobsByJobId(Long jobId);

    /**
     * 查询职位发布列表
     * 
     * @param jobs 职位发布
     * @return 职位发布集合
     */
    public List<Jobs> selectJobsList(Jobs jobs);

    /**
     * 新增职位发布
     * 
     * @param jobs 职位发布
     * @return 结果
     */
    public int insertJobs(Jobs jobs);

    /**
     * 修改职位发布
     * 
     * @param jobs 职位发布
     * @return 结果
     */
    public int updateJobs(Jobs jobs);

    /**
     * 批量删除职位发布
     * 
     * @param jobIds 需要删除的职位发布主键集合
     * @return 结果
     */
    public int deleteJobsByJobIds(Long[] jobIds);

    /**
     * 删除职位发布信息
     * 
     * @param jobId 职位发布主键
     * @return 结果
     */
    public int deleteJobsByJobId(Long jobId);

    /**
     * 审核
     *
     * @param jobId 主键id
     * @param status 审核状态
     * @param comment 审核备注
     */
    String approveJob(Long jobId, String status, String comment);

    /**
     * 职位上下架
     *
     * @param jobId 主键id
     * @param visible 上下架值
     */
    String updateJobVisibility(Long jobId, Integer visible);

    /**
     * 用户职位列表
     *
     * @param jobs 查询条件
     */
    List<Jobs> selectCustomerJobsList(Jobs jobs);

    void orderJob(Long jobId, Long resumeId, JobApprovalRequest request);
}
