package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TagsMapper;
import com.ruoyi.system.domain.Tags;
import com.ruoyi.system.service.ITagsService;

/**
 * 标签Service业务层处理
 * 
 * @author ruoyi
 * @date 2025-01-14
 */
@Service
public class TagsServiceImpl implements ITagsService 
{
    @Autowired
    private TagsMapper tagsMapper;

    /**
     * 查询标签
     * 
     * @param tagId 标签主键
     * @return 标签
     */
    @Override
    public Tags selectTagsByTagId(Long tagId)
    {
        return tagsMapper.selectTagsByTagId(tagId);
    }

    /**
     * 查询标签列表
     * 
     * @param tags 标签
     * @return 标签
     */
    @Override
    public List<Tags> selectTagsList(Tags tags)
    {
        return tagsMapper.selectTagsList(tags);
    }

    /**
     * 新增标签
     * 
     * @param tags 标签
     * @return 结果
     */
    @Override
    public int insertTags(Tags tags)
    {
        return tagsMapper.insertTags(tags);
    }

    /**
     * 修改标签
     * 
     * @param tags 标签
     * @return 结果
     */
    @Override
    public int updateTags(Tags tags)
    {
        return tagsMapper.updateTags(tags);
    }

    /**
     * 批量删除标签
     * 
     * @param tagIds 需要删除的标签主键
     * @return 结果
     */
    @Override
    public int deleteTagsByTagIds(Long[] tagIds)
    {
        return tagsMapper.deleteTagsByTagIds(tagIds);
    }

    /**
     * 删除标签信息
     * 
     * @param tagId 标签主键
     * @return 结果
     */
    @Override
    public int deleteTagsByTagId(Long tagId)
    {
        return tagsMapper.deleteTagsByTagId(tagId);
    }
}
