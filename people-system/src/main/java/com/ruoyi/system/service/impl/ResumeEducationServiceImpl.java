package com.ruoyi.system.service.impl;

import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ResumeEducationMapper;
import com.ruoyi.system.domain.ResumeEducation;
import com.ruoyi.system.service.IResumeEducationService;

/**
 * 教育背景Service业务层处理
 * 
 * @author ruoyi
 * @date 2025-01-15
 */
@Service
public class ResumeEducationServiceImpl implements IResumeEducationService 
{
    @Autowired
    private ResumeEducationMapper resumeEducationMapper;

    /**
     * 查询教育背景
     * 
     * @param educationId 教育背景主键
     * @return 教育背景
     */
    @Override
    public ResumeEducation selectResumeEducationByEducationId(Long educationId)
    {
        return resumeEducationMapper.selectResumeEducationByEducationId(educationId);
    }

    /**
     * 查询教育背景列表
     * 
     * @param resumeEducation 教育背景
     * @return 教育背景
     */
    @Override
    public List<ResumeEducation> selectResumeEducationList(ResumeEducation resumeEducation)
    {
        return resumeEducationMapper.selectResumeEducationList(resumeEducation);
    }

    /**
     * 新增教育背景
     * 
     * @param resumeEducation 教育背景
     * @return 结果
     */
    @Override
    public int insertResumeEducation(ResumeEducation resumeEducation)
    {
        return resumeEducationMapper.insertResumeEducation(resumeEducation);
    }

    /**
     * 修改教育背景
     * 
     * @param resumeEducation 教育背景
     * @return 结果
     */
    @Override
    public int updateResumeEducation(ResumeEducation resumeEducation)
    {
        return resumeEducationMapper.updateResumeEducation(resumeEducation);
    }

    /**
     * 批量删除教育背景
     * 
     * @param educationIds 需要删除的教育背景主键
     * @return 结果
     */
    @Override
    public int deleteResumeEducationByEducationIds(Long[] educationIds)
    {
        return resumeEducationMapper.deleteResumeEducationByEducationIds(educationIds);
    }

    /**
     * 删除教育背景信息
     * 
     * @param educationId 教育背景主键
     * @return 结果
     */
    @Override
    public int deleteResumeEducationByEducationId(Long educationId)
    {
        return resumeEducationMapper.deleteResumeEducationByEducationId(educationId);
    }

    @Override
    public List<ResumeEducation> getByResumesId(Long resumeId) {
        return resumeEducationMapper.getByResumesId(resumeId);
    }

    @Override
    public void deleteByResumeId(Long resumeId) {
        resumeEducationMapper.deleteByResumeId(resumeId);
    }
}
