package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ResumesMapper;
import com.ruoyi.system.domain.Resumes;
import com.ruoyi.system.service.IResumesService;

/**
 * 简历Service业务层处理
 * 
 * @author ruoyi
 * @date 2025-01-15
 */
@Service
public class ResumesServiceImpl implements IResumesService 
{
    @Autowired
    private ResumesMapper resumesMapper;

    /**
     * 查询简历
     * 
     * @param resumeId 简历主键
     * @return 简历
     */
    @Override
    public Resumes selectResumesByResumeId(Long resumeId)
    {
        return resumesMapper.selectResumesByResumeId(resumeId);
    }

    /**
     * 查询简历列表
     * 
     * @param resumes 简历
     * @return 简历
     */
    @Override
    public List<Resumes> selectResumesList(Resumes resumes)
    {
        return resumesMapper.selectResumesList(resumes);
    }

    /**
     * 新增简历
     * 
     * @param resumes 简历
     * @return 结果
     */
    @Override
    public int insertResumes(Resumes resumes)
    {
        return resumesMapper.insertResumes(resumes);
    }

    /**
     * 修改简历
     * 
     * @param resumes 简历
     * @return 结果
     */
    @Override
    public int updateResumes(Resumes resumes)
    {
        return resumesMapper.updateResumes(resumes);
    }

    /**
     * 批量删除简历
     * 
     * @param resumeIds 需要删除的简历主键
     * @return 结果
     */
    @Override
    public int deleteResumesByResumeIds(Long[] resumeIds)
    {
        return resumesMapper.deleteResumesByResumeIds(resumeIds);
    }

    /**
     * 删除简历信息
     * 
     * @param resumeId 简历主键
     * @return 结果
     */
    @Override
    public int deleteResumesByResumeId(Long resumeId)
    {
        return resumesMapper.deleteResumesByResumeId(resumeId);
    }

    @Override
    public Resumes getByLoginUserId(Long sysUserId)
    {
        return resumesMapper.getByLoginUserId(sysUserId);
    }
}
