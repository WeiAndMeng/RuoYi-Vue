package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ResumeExperiences;

/**
 * 工作经历Service接口
 * 
 * @author ruoyi
 * @date 2025-01-15
 */
public interface IResumeExperiencesService 
{
    /**
     * 查询工作经历
     * 
     * @param experienceId 工作经历主键
     * @return 工作经历
     */
    public ResumeExperiences selectResumeExperiencesByExperienceId(Long experienceId);

    /**
     * 查询工作经历列表
     * 
     * @param resumeExperiences 工作经历
     * @return 工作经历集合
     */
    public List<ResumeExperiences> selectResumeExperiencesList(ResumeExperiences resumeExperiences);

    /**
     * 新增工作经历
     * 
     * @param resumeExperiences 工作经历
     * @return 结果
     */
    public int insertResumeExperiences(ResumeExperiences resumeExperiences);

    /**
     * 修改工作经历
     * 
     * @param resumeExperiences 工作经历
     * @return 结果
     */
    public int updateResumeExperiences(ResumeExperiences resumeExperiences);

    /**
     * 批量删除工作经历
     * 
     * @param experienceIds 需要删除的工作经历主键集合
     * @return 结果
     */
    public int deleteResumeExperiencesByExperienceIds(Long[] experienceIds);

    /**
     * 删除工作经历信息
     * 
     * @param experienceId 工作经历主键
     * @return 结果
     */
    public int deleteResumeExperiencesByExperienceId(Long experienceId);

    List<ResumeExperiences> getByResumesId(Long resumeId);

    void deleteByResumeId(Long resumeId);
}
