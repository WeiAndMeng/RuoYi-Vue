package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TbLikeMapper;
import com.ruoyi.system.domain.TbLike;
import com.ruoyi.system.service.ITbLikeService;

/**
 * 点赞Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-30
 */
@Service
public class TbLikeServiceImpl implements ITbLikeService 
{
    @Autowired
    private TbLikeMapper tbLikeMapper;

    /**
     * 查询点赞
     * 
     * @param id 点赞主键
     * @return 点赞
     */
    @Override
    public TbLike selectTbLikeById(Long id)
    {
        return tbLikeMapper.selectTbLikeById(id);
    }

    /**
     * 查询点赞列表
     * 
     * @param tbLike 点赞
     * @return 点赞
     */
    @Override
    public List<TbLike> selectTbLikeList(TbLike tbLike)
    {
        return tbLikeMapper.selectTbLikeList(tbLike);
    }

    /**
     * 新增点赞
     * 
     * @param tbLike 点赞
     * @return 结果
     */
    @Override
    public int insertTbLike(TbLike tbLike)
    {
        tbLike.setCreateTime(DateUtils.getNowDate());
        return tbLikeMapper.insertTbLike(tbLike);
    }

    /**
     * 修改点赞
     * 
     * @param tbLike 点赞
     * @return 结果
     */
    @Override
    public int updateTbLike(TbLike tbLike)
    {
        return tbLikeMapper.updateTbLike(tbLike);
    }

    /**
     * 批量删除点赞
     * 
     * @param ids 需要删除的点赞主键
     * @return 结果
     */
    @Override
    public int deleteTbLikeByIds(Long[] ids)
    {
        return tbLikeMapper.deleteTbLikeByIds(ids);
    }

    /**
     * 删除点赞信息
     * 
     * @param id 点赞主键
     * @return 结果
     */
    @Override
    public int deleteTbLikeById(Long id)
    {
        return tbLikeMapper.deleteTbLikeById(id);
    }
}
