package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ResumeSkills;

/**
 * 技能证书Service接口
 * 
 * @author ruoyi
 * @date 2025-01-15
 */
public interface IResumeSkillsService 
{
    /**
     * 查询技能证书
     * 
     * @param skillId 技能证书主键
     * @return 技能证书
     */
    public ResumeSkills selectResumeSkillsBySkillId(Long skillId);

    /**
     * 查询技能证书列表
     * 
     * @param resumeSkills 技能证书
     * @return 技能证书集合
     */
    public List<ResumeSkills> selectResumeSkillsList(ResumeSkills resumeSkills);

    /**
     * 新增技能证书
     * 
     * @param resumeSkills 技能证书
     * @return 结果
     */
    public int insertResumeSkills(ResumeSkills resumeSkills);

    /**
     * 修改技能证书
     * 
     * @param resumeSkills 技能证书
     * @return 结果
     */
    public int updateResumeSkills(ResumeSkills resumeSkills);

    /**
     * 批量删除技能证书
     * 
     * @param skillIds 需要删除的技能证书主键集合
     * @return 结果
     */
    public int deleteResumeSkillsBySkillIds(Long[] skillIds);

    /**
     * 删除技能证书信息
     * 
     * @param skillId 技能证书主键
     * @return 结果
     */
    public int deleteResumeSkillsBySkillId(Long skillId);

    List<ResumeSkills> getByResumesId(Long resumeId);

    void deleteByResumeId(Long resumeId);

}
