package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.ResumeTemplates;

/**
 * 简历模板Service接口
 *
 * @author ruoyi
 * @date 2025-01-15
 */
public interface IResumeTemplatesService
{
    /**
     * 查询简历模板
     *
     * @param templateId 简历模板主键
     * @return 简历模板
     */
    public ResumeTemplates selectResumeTemplatesByTemplateId(Long templateId);

    /**
     * 查询简历模板列表
     *
     * @param resumeTemplates 简历模板
     * @return 简历模板集合
     */
    public List<ResumeTemplates> selectResumeTemplatesList(ResumeTemplates resumeTemplates);

    /**
     * 新增简历模板
     *
     * @param resumeTemplates 简历模板
     * @return 结果
     */
    public int insertResumeTemplates(ResumeTemplates resumeTemplates);

    /**
     * 修改简历模板
     *
     * @param resumeTemplates 简历模板
     * @return 结果
     */
    public int updateResumeTemplates(ResumeTemplates resumeTemplates);

    /**
     * 批量删除简历模板
     *
     * @param templateIds 需要删除的简历模板主键集合
     * @return 结果
     */
    public int deleteResumeTemplatesByTemplateIds(Long[] templateIds);

    /**
     * 删除简历模板信息
     *
     * @param templateId 简历模板主键
     * @return 结果
     */
    public int deleteResumeTemplatesByTemplateId(Long templateId);

}
