package com.ruoyi.system.service.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.ruoyi.common.enums.JobStatus;
import com.ruoyi.system.domain.vo.JobApprovalRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.JobsMapper;
import com.ruoyi.system.domain.Jobs;
import com.ruoyi.system.service.IJobsService;

/**
 * 职位发布Service业务层处理
 * 
 * @author ruoyi
 * @date 2025-01-14
 */
@Service
public class JobsServiceImpl implements IJobsService 
{
    @Autowired
    private JobsMapper jobsMapper;

    /**
     * 查询职位发布
     * 
     * @param jobId 职位发布主键
     * @return 职位发布
     */
    @Override
    public Jobs selectJobsByJobId(Long jobId)
    {
        return jobsMapper.selectJobsByJobId(jobId);
    }

    /**
     * 查询职位发布列表
     * 
     * @param jobs 职位发布
     * @return 职位发布
     */
    @Override
    public List<Jobs> selectJobsList(Jobs jobs)
    {
        return jobsMapper.selectJobsList(jobs);
    }

    /**
     * 新增职位发布
     * 
     * @param jobs 职位发布
     * @return 结果
     */
    @Override
    public int insertJobs(Jobs jobs)
    {
        return jobsMapper.insertJobs(jobs);
    }

    /**
     * 修改职位发布
     * 
     * @param jobs 职位发布
     * @return 结果
     */
    @Override
    public int updateJobs(Jobs jobs)
    {
        return jobsMapper.updateJobs(jobs);
    }

    /**
     * 批量删除职位发布
     * 
     * @param jobIds 需要删除的职位发布主键
     * @return 结果
     */
    @Override
    public int deleteJobsByJobIds(Long[] jobIds)
    {
        return jobsMapper.deleteJobsByJobIds(jobIds);
    }

    /**
     * 删除职位发布信息
     * 
     * @param jobId 职位发布主键
     * @return 结果
     */
    @Override
    public int deleteJobsByJobId(Long jobId)
    {
        return jobsMapper.deleteJobsByJobId(jobId);
    }

    @Override
    public String approveJob(Long jobId, String status, String comment) {
        Jobs job = jobsMapper.selectJobsByJobId(jobId);
        if (job == null) {
            return "职位不存在！";
        }
        if (!JobStatus.PENDING.name().equals(job.getStatus())) {
            return "只能审核待审核的职位！";
        }

        if (!Arrays.asList("APPROVED", "REJECTED").contains(status)) {
            return "非法操作！";
        }

        job.setStatus(status);
        job.setUpdatedAt(new Date());
        jobsMapper.updateJobs(job);
        return null;
    }

    @Override
    public String updateJobVisibility(Long jobId, Integer visible) {
        Jobs job = jobsMapper.selectJobsByJobId(jobId);
        if (job == null) {
            return "职位不存在！";
        }
        if (visible == 1 && !JobStatus.APPROVED.name().equals(job.getStatus())) {
            return "已审核成功职位才可上架";
        }
        job.setVisible(visible);
        job.setUpdatedAt(new Date());
        jobsMapper.updateJobs(job);
        return null;
    }

    @Override
    public List<Jobs> selectCustomerJobsList(Jobs jobs) {
        return jobsMapper.selectCustomerJobsList(jobs);
    }

    @Override
    public void orderJob(Long jobId, Long resumeId, JobApprovalRequest request) {
        jobsMapper.orderJob(jobId, resumeId, request);
    }
}
