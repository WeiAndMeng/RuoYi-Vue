package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ResumeEducation;

/**
 * 教育背景Service接口
 * 
 * @author ruoyi
 * @date 2025-01-15
 */
public interface IResumeEducationService 
{
    /**
     * 查询教育背景
     * 
     * @param educationId 教育背景主键
     * @return 教育背景
     */
    public ResumeEducation selectResumeEducationByEducationId(Long educationId);

    /**
     * 查询教育背景列表
     * 
     * @param resumeEducation 教育背景
     * @return 教育背景集合
     */
    public List<ResumeEducation> selectResumeEducationList(ResumeEducation resumeEducation);

    /**
     * 新增教育背景
     * 
     * @param resumeEducation 教育背景
     * @return 结果
     */
    public int insertResumeEducation(ResumeEducation resumeEducation);

    /**
     * 修改教育背景
     * 
     * @param resumeEducation 教育背景
     * @return 结果
     */
    public int updateResumeEducation(ResumeEducation resumeEducation);

    /**
     * 批量删除教育背景
     * 
     * @param educationIds 需要删除的教育背景主键集合
     * @return 结果
     */
    public int deleteResumeEducationByEducationIds(Long[] educationIds);

    /**
     * 删除教育背景信息
     * 
     * @param educationId 教育背景主键
     * @return 结果
     */
    public int deleteResumeEducationByEducationId(Long educationId);

    List<ResumeEducation> getByResumesId(Long resumeId);

    void deleteByResumeId(Long resumeId);

}
