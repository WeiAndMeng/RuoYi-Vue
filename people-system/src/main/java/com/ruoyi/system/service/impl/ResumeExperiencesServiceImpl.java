package com.ruoyi.system.service.impl;

import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ResumeExperiencesMapper;
import com.ruoyi.system.domain.ResumeExperiences;
import com.ruoyi.system.service.IResumeExperiencesService;

/**
 * 工作经历Service业务层处理
 * 
 * @author ruoyi
 * @date 2025-01-15
 */
@Service
public class ResumeExperiencesServiceImpl implements IResumeExperiencesService 
{
    @Autowired
    private ResumeExperiencesMapper resumeExperiencesMapper;

    /**
     * 查询工作经历
     * 
     * @param experienceId 工作经历主键
     * @return 工作经历
     */
    @Override
    public ResumeExperiences selectResumeExperiencesByExperienceId(Long experienceId)
    {
        return resumeExperiencesMapper.selectResumeExperiencesByExperienceId(experienceId);
    }

    /**
     * 查询工作经历列表
     * 
     * @param resumeExperiences 工作经历
     * @return 工作经历
     */
    @Override
    public List<ResumeExperiences> selectResumeExperiencesList(ResumeExperiences resumeExperiences)
    {
        return resumeExperiencesMapper.selectResumeExperiencesList(resumeExperiences);
    }

    /**
     * 新增工作经历
     * 
     * @param resumeExperiences 工作经历
     * @return 结果
     */
    @Override
    public int insertResumeExperiences(ResumeExperiences resumeExperiences)
    {
        return resumeExperiencesMapper.insertResumeExperiences(resumeExperiences);
    }

    /**
     * 修改工作经历
     * 
     * @param resumeExperiences 工作经历
     * @return 结果
     */
    @Override
    public int updateResumeExperiences(ResumeExperiences resumeExperiences)
    {
        return resumeExperiencesMapper.updateResumeExperiences(resumeExperiences);
    }

    /**
     * 批量删除工作经历
     * 
     * @param experienceIds 需要删除的工作经历主键
     * @return 结果
     */
    @Override
    public int deleteResumeExperiencesByExperienceIds(Long[] experienceIds)
    {
        return resumeExperiencesMapper.deleteResumeExperiencesByExperienceIds(experienceIds);
    }

    /**
     * 删除工作经历信息
     * 
     * @param experienceId 工作经历主键
     * @return 结果
     */
    @Override
    public int deleteResumeExperiencesByExperienceId(Long experienceId)
    {
        return resumeExperiencesMapper.deleteResumeExperiencesByExperienceId(experienceId);
    }

    @Override
    public List<ResumeExperiences> getByResumesId(Long resumeId) {
        return resumeExperiencesMapper.getByResumesId(resumeId);
    }

    @Override
    public void deleteByResumeId(Long resumeId) {
        resumeExperiencesMapper.deleteByResumeId(resumeId);
    }
}
