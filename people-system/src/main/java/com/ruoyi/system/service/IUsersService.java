package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Users;

/**
 * 存储用户的基本信息Service接口
 * 
 * @author ruoyi
 * @date 2025-01-13
 */
public interface IUsersService 
{
    /**
     * 查询存储用户的基本信息
     * 
     * @param userId 存储用户的基本信息主键
     * @return 存储用户的基本信息
     */
    public Users selectUsersByUserId(Long userId);

    /**
     * 查询存储用户的基本信息列表
     * 
     * @param users 存储用户的基本信息
     * @return 存储用户的基本信息集合
     */
    public List<Users> selectUsersList(Users users);

    /**
     * 新增存储用户的基本信息
     * 
     * @param users 存储用户的基本信息
     * @return 结果
     */
    public int insertUsers(Users users);

    /**
     * 修改存储用户的基本信息
     * 
     * @param users 存储用户的基本信息
     * @return 结果
     */
    public int updateUsers(Users users);

    /**
     * 批量删除存储用户的基本信息
     * 
     * @param userIds 需要删除的存储用户的基本信息主键集合
     * @return 结果
     */
    public int deleteUsersByUserIds(Long[] userIds);

    /**
     * 删除存储用户的基本信息信息
     * 
     * @param userId 存储用户的基本信息主键
     * @return 结果
     */
    public int deleteUsersByUserId(Long userId);

    /**
     * 获取登录用户信息
     *
     * @param sysUserId 登录用户id
     * @return 结果
     */
    Users getByLoginUserId(Long sysUserId);
}
