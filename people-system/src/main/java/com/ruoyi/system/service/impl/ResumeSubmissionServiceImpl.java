package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.Jobs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ResumeSubmissionMapper;
import com.ruoyi.system.domain.ResumeSubmission;
import com.ruoyi.system.service.IResumeSubmissionService;

/**
 * 简历投递记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2025-01-18
 */
@Service
public class ResumeSubmissionServiceImpl implements IResumeSubmissionService 
{
    @Autowired
    private ResumeSubmissionMapper resumeSubmissionMapper;

    /**
     * 查询简历投递记录
     * 
     * @param id 简历投递记录主键
     * @return 简历投递记录
     */
    @Override
    public ResumeSubmission selectResumeSubmissionById(Long id)
    {
        return resumeSubmissionMapper.selectResumeSubmissionById(id);
    }

    /**
     * 查询简历投递记录列表
     * 
     * @param resumeSubmission 简历投递记录
     * @return 简历投递记录
     */
    @Override
    public List<ResumeSubmission> selectResumeSubmissionList(ResumeSubmission resumeSubmission)
    {
        return resumeSubmissionMapper.selectResumeSubmissionList(resumeSubmission);
    }

    /**
     * 新增简历投递记录
     * 
     * @param resumeSubmission 简历投递记录
     * @return 结果
     */
    @Override
    public int insertResumeSubmission(ResumeSubmission resumeSubmission)
    {
        return resumeSubmissionMapper.insertResumeSubmission(resumeSubmission);
    }

    /**
     * 修改简历投递记录
     * 
     * @param resumeSubmission 简历投递记录
     * @return 结果
     */
    @Override
    public int updateResumeSubmission(ResumeSubmission resumeSubmission)
    {
        return resumeSubmissionMapper.updateResumeSubmission(resumeSubmission);
    }

    /**
     * 批量删除简历投递记录
     * 
     * @param ids 需要删除的简历投递记录主键
     * @return 结果
     */
    @Override
    public int deleteResumeSubmissionByIds(Long[] ids)
    {
        return resumeSubmissionMapper.deleteResumeSubmissionByIds(ids);
    }

    /**
     * 删除简历投递记录信息
     * 
     * @param id 简历投递记录主键
     * @return 结果
     */
    @Override
    public int deleteResumeSubmissionById(Long id)
    {
        return resumeSubmissionMapper.deleteResumeSubmissionById(id);
    }

    @Override
    public ResumeSubmission selectByResumeIdAndJobId(Long resumeId, Long jobId) {
        return resumeSubmissionMapper.selectByResumeIdAndJobId(resumeId, jobId);
    }
}
