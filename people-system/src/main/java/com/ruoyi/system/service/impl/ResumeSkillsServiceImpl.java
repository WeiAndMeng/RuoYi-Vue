package com.ruoyi.system.service.impl;

import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ResumeSkillsMapper;
import com.ruoyi.system.domain.ResumeSkills;
import com.ruoyi.system.service.IResumeSkillsService;

/**
 * 技能证书Service业务层处理
 * 
 * @author ruoyi
 * @date 2025-01-15
 */
@Service
public class ResumeSkillsServiceImpl implements IResumeSkillsService 
{
    @Autowired
    private ResumeSkillsMapper resumeSkillsMapper;

    /**
     * 查询技能证书
     * 
     * @param skillId 技能证书主键
     * @return 技能证书
     */
    @Override
    public ResumeSkills selectResumeSkillsBySkillId(Long skillId)
    {
        return resumeSkillsMapper.selectResumeSkillsBySkillId(skillId);
    }

    /**
     * 查询技能证书列表
     * 
     * @param resumeSkills 技能证书
     * @return 技能证书
     */
    @Override
    public List<ResumeSkills> selectResumeSkillsList(ResumeSkills resumeSkills)
    {
        return resumeSkillsMapper.selectResumeSkillsList(resumeSkills);
    }

    /**
     * 新增技能证书
     * 
     * @param resumeSkills 技能证书
     * @return 结果
     */
    @Override
    public int insertResumeSkills(ResumeSkills resumeSkills)
    {
        return resumeSkillsMapper.insertResumeSkills(resumeSkills);
    }

    /**
     * 修改技能证书
     * 
     * @param resumeSkills 技能证书
     * @return 结果
     */
    @Override
    public int updateResumeSkills(ResumeSkills resumeSkills)
    {
        return resumeSkillsMapper.updateResumeSkills(resumeSkills);
    }

    /**
     * 批量删除技能证书
     * 
     * @param skillIds 需要删除的技能证书主键
     * @return 结果
     */
    @Override
    public int deleteResumeSkillsBySkillIds(Long[] skillIds)
    {
        return resumeSkillsMapper.deleteResumeSkillsBySkillIds(skillIds);
    }

    /**
     * 删除技能证书信息
     * 
     * @param skillId 技能证书主键
     * @return 结果
     */
    @Override
    public int deleteResumeSkillsBySkillId(Long skillId)
    {
        return resumeSkillsMapper.deleteResumeSkillsBySkillId(skillId);
    }

    @Override
    public List<ResumeSkills> getByResumesId(Long resumeId) {
        return resumeSkillsMapper.getByResumesId(resumeId);
    }

    @Override
    public void deleteByResumeId(Long resumeId) {
        resumeSkillsMapper.deleteByResumeId(resumeId);
    }
}
