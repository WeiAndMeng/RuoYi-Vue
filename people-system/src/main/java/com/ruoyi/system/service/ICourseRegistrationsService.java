package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.CourseRegistrations;
import com.ruoyi.system.domain.Courses;

/**
 * 课程报名记录，记录用户报名信息及支付状态Service接口
 * 
 * @author ruoyi
 * @date 2025-01-21
 */
public interface ICourseRegistrationsService 
{
    /**
     * 查询课程报名记录，记录用户报名信息及支付状态
     * 
     * @param registrationId 课程报名记录，记录用户报名信息及支付状态主键
     * @return 课程报名记录，记录用户报名信息及支付状态
     */
    public CourseRegistrations selectCourseRegistrationsByRegistrationId(Long registrationId);

    /**
     * 查询课程报名记录，记录用户报名信息及支付状态列表
     * 
     * @param courseRegistrations 课程报名记录，记录用户报名信息及支付状态
     * @return 课程报名记录，记录用户报名信息及支付状态集合
     */
    public List<CourseRegistrations> selectCourseRegistrationsList(CourseRegistrations courseRegistrations);

    /**
     * 新增课程报名记录，记录用户报名信息及支付状态
     * 
     * @param courseRegistrations 课程报名记录，记录用户报名信息及支付状态
     * @return 结果
     */
    public int insertCourseRegistrations(CourseRegistrations courseRegistrations);

    /**
     * 修改课程报名记录，记录用户报名信息及支付状态
     * 
     * @param courseRegistrations 课程报名记录，记录用户报名信息及支付状态
     * @return 结果
     */
    public int updateCourseRegistrations(CourseRegistrations courseRegistrations);

    /**
     * 批量删除课程报名记录，记录用户报名信息及支付状态
     * 
     * @param registrationIds 需要删除的课程报名记录，记录用户报名信息及支付状态主键集合
     * @return 结果
     */
    public int deleteCourseRegistrationsByRegistrationIds(Long[] registrationIds);

    /**
     * 删除课程报名记录，记录用户报名信息及支付状态信息
     * 
     * @param registrationId 课程报名记录，记录用户报名信息及支付状态主键
     * @return 结果
     */
    public int deleteCourseRegistrationsByRegistrationId(Long registrationId);

    int addUserCourse(Courses courses, Long userId);
}
