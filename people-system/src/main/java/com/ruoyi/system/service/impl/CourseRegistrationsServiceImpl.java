package com.ruoyi.system.service.impl;
import java.util.Date;

import java.util.List;

import com.ruoyi.system.domain.Courses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CourseRegistrationsMapper;
import com.ruoyi.system.domain.CourseRegistrations;
import com.ruoyi.system.service.ICourseRegistrationsService;

/**
 * 课程报名记录，记录用户报名信息及支付状态Service业务层处理
 * 
 * @author ruoyi
 * @date 2025-01-21
 */
@Service
public class CourseRegistrationsServiceImpl implements ICourseRegistrationsService 
{
    @Autowired
    private CourseRegistrationsMapper courseRegistrationsMapper;

    /**
     * 查询课程报名记录，记录用户报名信息及支付状态
     * 
     * @param registrationId 课程报名记录，记录用户报名信息及支付状态主键
     * @return 课程报名记录，记录用户报名信息及支付状态
     */
    @Override
    public CourseRegistrations selectCourseRegistrationsByRegistrationId(Long registrationId)
    {
        return courseRegistrationsMapper.selectCourseRegistrationsByRegistrationId(registrationId);
    }

    /**
     * 查询课程报名记录，记录用户报名信息及支付状态列表
     * 
     * @param courseRegistrations 课程报名记录，记录用户报名信息及支付状态
     * @return 课程报名记录，记录用户报名信息及支付状态
     */
    @Override
    public List<CourseRegistrations> selectCourseRegistrationsList(CourseRegistrations courseRegistrations)
    {
        return courseRegistrationsMapper.selectCourseRegistrationsList(courseRegistrations);
    }

    /**
     * 新增课程报名记录，记录用户报名信息及支付状态
     * 
     * @param courseRegistrations 课程报名记录，记录用户报名信息及支付状态
     * @return 结果
     */
    @Override
    public int insertCourseRegistrations(CourseRegistrations courseRegistrations)
    {
        return courseRegistrationsMapper.insertCourseRegistrations(courseRegistrations);
    }

    /**
     * 修改课程报名记录，记录用户报名信息及支付状态
     * 
     * @param courseRegistrations 课程报名记录，记录用户报名信息及支付状态
     * @return 结果
     */
    @Override
    public int updateCourseRegistrations(CourseRegistrations courseRegistrations)
    {
        return courseRegistrationsMapper.updateCourseRegistrations(courseRegistrations);
    }

    /**
     * 批量删除课程报名记录，记录用户报名信息及支付状态
     * 
     * @param registrationIds 需要删除的课程报名记录，记录用户报名信息及支付状态主键
     * @return 结果
     */
    @Override
    public int deleteCourseRegistrationsByRegistrationIds(Long[] registrationIds)
    {
        return courseRegistrationsMapper.deleteCourseRegistrationsByRegistrationIds(registrationIds);
    }

    /**
     * 删除课程报名记录，记录用户报名信息及支付状态信息
     * 
     * @param registrationId 课程报名记录，记录用户报名信息及支付状态主键
     * @return 结果
     */
    @Override
    public int deleteCourseRegistrationsByRegistrationId(Long registrationId)
    {
        return courseRegistrationsMapper.deleteCourseRegistrationsByRegistrationId(registrationId);
    }

    @Override
    public int addUserCourse(Courses courses, Long userId) {
        CourseRegistrations courseRegistrations = new CourseRegistrations();
        courseRegistrations.setSysUserId(userId);
        courseRegistrations.setCourseId(courses.getCourseId());
        courseRegistrations.setPaymentStatus(0L);
        courseRegistrations.setCreatedAt(new Date());
        courseRegistrations.setUpdatedAt(new Date());
        return courseRegistrationsMapper.insertCourseRegistrations(courseRegistrations);
    }
}
