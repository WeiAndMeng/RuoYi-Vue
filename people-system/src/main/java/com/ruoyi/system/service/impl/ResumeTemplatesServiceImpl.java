package com.ruoyi.system.service.impl;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.core.domain.AjaxResult;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ResumeTemplatesMapper;
import com.ruoyi.system.domain.ResumeTemplates;
import com.ruoyi.system.service.IResumeTemplatesService;

/**
 * 简历模板Service业务层处理
 *
 * @author ruoyi
 * @date 2025-01-15
 */
@Service
public class ResumeTemplatesServiceImpl implements IResumeTemplatesService
{
    @Autowired
    private ResumeTemplatesMapper resumeTemplatesMapper;

    /**
     * 查询简历模板
     *
     * @param templateId 简历模板主键
     * @return 简历模板
     */
    @Override
    public ResumeTemplates selectResumeTemplatesByTemplateId(Long templateId)
    {
        return resumeTemplatesMapper.selectResumeTemplatesByTemplateId(templateId);
    }

    /**
     * 查询简历模板列表
     *
     * @param resumeTemplates 简历模板
     * @return 简历模板
     */
    @Override
    public List<ResumeTemplates> selectResumeTemplatesList(ResumeTemplates resumeTemplates)
    {
        return resumeTemplatesMapper.selectResumeTemplatesList(resumeTemplates);
    }

    /**
     * 新增简历模板
     *
     * @param resumeTemplates 简历模板
     * @return 结果
     */
    @Override
    public int insertResumeTemplates(ResumeTemplates resumeTemplates)
    {
        return resumeTemplatesMapper.insertResumeTemplates(resumeTemplates);
    }

    /**
     * 修改简历模板
     *
     * @param resumeTemplates 简历模板
     * @return 结果
     */
    @Override
    public int updateResumeTemplates(ResumeTemplates resumeTemplates)
    {
        return resumeTemplatesMapper.updateResumeTemplates(resumeTemplates);
    }

    /**
     * 批量删除简历模板
     *
     * @param templateIds 需要删除的简历模板主键
     * @return 结果
     */
    @Override
    public int deleteResumeTemplatesByTemplateIds(Long[] templateIds)
    {
        return resumeTemplatesMapper.deleteResumeTemplatesByTemplateIds(templateIds);
    }

    /**
     * 删除简历模板信息
     *
     * @param templateId 简历模板主键
     * @return 结果
     */
    @Override
    public int deleteResumeTemplatesByTemplateId(Long templateId)
    {
        return resumeTemplatesMapper.deleteResumeTemplatesByTemplateId(templateId);
    }

}
