package com.ruoyi.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UsersMapper;
import com.ruoyi.system.domain.Users;
import com.ruoyi.system.service.IUsersService;

/**
 * 存储用户的基本信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2025-01-13
 */
@Service
public class UsersServiceImpl implements IUsersService 
{
    @Autowired
    private UsersMapper usersMapper;

    /**
     * 查询存储用户的基本信息
     * 
     * @param userId 存储用户的基本信息主键
     * @return 存储用户的基本信息
     */
    @Override
    public Users selectUsersByUserId(Long userId)
    {
        return usersMapper.selectUsersByUserId(userId);
    }

    /**
     * 查询存储用户的基本信息列表
     * 
     * @param users 存储用户的基本信息
     * @return 存储用户的基本信息
     */
    @Override
    public List<Users> selectUsersList(Users users)
    {
        return usersMapper.selectUsersList(users);
    }

    /**
     * 新增存储用户的基本信息
     * 
     * @param users 存储用户的基本信息
     * @return 结果
     */
    @Override
    public int insertUsers(Users users)
    {
        return usersMapper.insertUsers(users);
    }

    /**
     * 修改存储用户的基本信息
     * 
     * @param users 存储用户的基本信息
     * @return 结果
     */
    @Override
    public int updateUsers(Users users)
    {
        return usersMapper.updateUsers(users);
    }

    /**
     * 批量删除存储用户的基本信息
     * 
     * @param userIds 需要删除的存储用户的基本信息主键
     * @return 结果
     */
    @Override
    public int deleteUsersByUserIds(Long[] userIds)
    {
        return usersMapper.deleteUsersByUserIds(userIds);
    }

    /**
     * 删除存储用户的基本信息信息
     * 
     * @param userId 存储用户的基本信息主键
     * @return 结果
     */
    @Override
    public int deleteUsersByUserId(Long userId)
    {
        return usersMapper.deleteUsersByUserId(userId);
    }

    @Override
    public Users getByLoginUserId(Long sysUserId) {
        return usersMapper.getByLoginUserId(sysUserId);
    }
}
