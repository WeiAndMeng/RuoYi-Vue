package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TbLike;

/**
 * 点赞Mapper接口
 * 
 * @author ruoyi
 * @date 2024-03-30
 */
public interface TbLikeMapper 
{
    /**
     * 查询点赞
     * 
     * @param id 点赞主键
     * @return 点赞
     */
    public TbLike selectTbLikeById(Long id);

    /**
     * 查询点赞列表
     * 
     * @param tbLike 点赞
     * @return 点赞集合
     */
    public List<TbLike> selectTbLikeList(TbLike tbLike);

    /**
     * 新增点赞
     * 
     * @param tbLike 点赞
     * @return 结果
     */
    public int insertTbLike(TbLike tbLike);

    /**
     * 修改点赞
     * 
     * @param tbLike 点赞
     * @return 结果
     */
    public int updateTbLike(TbLike tbLike);

    /**
     * 删除点赞
     * 
     * @param id 点赞主键
     * @return 结果
     */
    public int deleteTbLikeById(Long id);

    /**
     * 批量删除点赞
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTbLikeByIds(Long[] ids);
}
