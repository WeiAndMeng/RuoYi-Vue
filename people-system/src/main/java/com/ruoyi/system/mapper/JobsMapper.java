package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Jobs;
import com.ruoyi.system.domain.vo.JobApprovalRequest;
import org.apache.ibatis.annotations.Param;

/**
 * 职位发布Mapper接口
 * 
 * @author ruoyi
 * @date 2025-01-14
 */
public interface JobsMapper 
{
    /**
     * 查询职位发布
     * 
     * @param jobId 职位发布主键
     * @return 职位发布
     */
    public Jobs selectJobsByJobId(Long jobId);

    /**
     * 查询职位发布列表
     * 
     * @param jobs 职位发布
     * @return 职位发布集合
     */
    public List<Jobs> selectJobsList(Jobs jobs);

    /**
     * 新增职位发布
     * 
     * @param jobs 职位发布
     * @return 结果
     */
    public int insertJobs(Jobs jobs);

    /**
     * 修改职位发布
     * 
     * @param jobs 职位发布
     * @return 结果
     */
    public int updateJobs(Jobs jobs);

    /**
     * 删除职位发布
     * 
     * @param jobId 职位发布主键
     * @return 结果
     */
    public int deleteJobsByJobId(Long jobId);

    /**
     * 批量删除职位发布
     * 
     * @param jobIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteJobsByJobIds(Long[] jobIds);

    /**
     * 用户职位列表
     *
     * @param jobs 查询条件
     */
    List<Jobs> selectCustomerJobsList(Jobs jobs);

    void orderJob(@Param("jobId") Long jobId, @Param("resumeId") Long resumeId, @Param("request") JobApprovalRequest request);
}
