package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Resumes;

/**
 * 简历Mapper接口
 * 
 * @author ruoyi
 * @date 2025-01-15
 */
public interface ResumesMapper 
{
    /**
     * 查询简历
     * 
     * @param resumeId 简历主键
     * @return 简历
     */
    public Resumes selectResumesByResumeId(Long resumeId);

    /**
     * 查询简历列表
     * 
     * @param resumes 简历
     * @return 简历集合
     */
    public List<Resumes> selectResumesList(Resumes resumes);

    /**
     * 新增简历
     * 
     * @param resumes 简历
     * @return 结果
     */
    public int insertResumes(Resumes resumes);

    /**
     * 修改简历
     * 
     * @param resumes 简历
     * @return 结果
     */
    public int updateResumes(Resumes resumes);

    /**
     * 删除简历
     * 
     * @param resumeId 简历主键
     * @return 结果
     */
    public int deleteResumesByResumeId(Long resumeId);

    /**
     * 批量删除简历
     * 
     * @param resumeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteResumesByResumeIds(Long[] resumeIds);

    Resumes getByLoginUserId(Long sysUserId);
}
