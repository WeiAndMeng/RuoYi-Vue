package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ResumeTemplates;

/**
 * 简历模板Mapper接口
 *
 * @author ruoyi
 * @date 2025-01-15
 */
public interface ResumeTemplatesMapper
{
    /**
     * 查询简历模板
     *
     * @param templateId 简历模板主键
     * @return 简历模板
     */
    public ResumeTemplates selectResumeTemplatesByTemplateId(Long templateId);

    /**
     * 查询简历模板列表
     *
     * @param resumeTemplates 简历模板
     * @return 简历模板集合
     */
    public List<ResumeTemplates> selectResumeTemplatesList(ResumeTemplates resumeTemplates);

    /**
     * 新增简历模板
     *
     * @param resumeTemplates 简历模板
     * @return 结果
     */
    public int insertResumeTemplates(ResumeTemplates resumeTemplates);

    /**
     * 修改简历模板
     *
     * @param resumeTemplates 简历模板
     * @return 结果
     */
    public int updateResumeTemplates(ResumeTemplates resumeTemplates);

    /**
     * 删除简历模板
     *
     * @param templateId 简历模板主键
     * @return 结果
     */
    public int deleteResumeTemplatesByTemplateId(Long templateId);

    /**
     * 批量删除简历模板
     *
     * @param templateIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteResumeTemplatesByTemplateIds(Long[] templateIds);
}
