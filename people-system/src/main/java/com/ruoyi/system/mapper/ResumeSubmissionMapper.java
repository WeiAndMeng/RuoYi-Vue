package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.system.domain.Jobs;
import com.ruoyi.system.domain.ResumeSubmission;
import org.apache.ibatis.annotations.Param;

/**
 * 简历投递记录Mapper接口
 * 
 * @author ruoyi
 * @date 2025-01-18
 */
public interface ResumeSubmissionMapper 
{
    /**
     * 查询简历投递记录
     * 
     * @param id 简历投递记录主键
     * @return 简历投递记录
     */
    public ResumeSubmission selectResumeSubmissionById(Long id);

    /**
     * 查询简历投递记录列表
     * 
     * @param resumeSubmission 简历投递记录
     * @return 简历投递记录集合
     */
    public List<ResumeSubmission> selectResumeSubmissionList(ResumeSubmission resumeSubmission);

    /**
     * 新增简历投递记录
     * 
     * @param resumeSubmission 简历投递记录
     * @return 结果
     */
    public int insertResumeSubmission(ResumeSubmission resumeSubmission);

    /**
     * 修改简历投递记录
     * 
     * @param resumeSubmission 简历投递记录
     * @return 结果
     */
    public int updateResumeSubmission(ResumeSubmission resumeSubmission);

    /**
     * 删除简历投递记录
     * 
     * @param id 简历投递记录主键
     * @return 结果
     */
    public int deleteResumeSubmissionById(Long id);

    /**
     * 批量删除简历投递记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteResumeSubmissionByIds(Long[] ids);

    ResumeSubmission selectByResumeIdAndJobId(@Param("resumeId") Long resumeId, @Param("jobId") Long jobId);
}
