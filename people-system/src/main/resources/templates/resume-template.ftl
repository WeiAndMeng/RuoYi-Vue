<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${title}</title>
    <style>
        body {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            background-color: #f4f4f9;
            margin: 0;
            padding: 0;
            color: #333;
        }
        .container {
            width: 70%;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 4px 20px rgba(0, 0, 0, 0.1);
            background: linear-gradient(to bottom right, #e0f7fa, #b2ebf2);
        }
        .header {
            text-align: center;
            margin-bottom: 40px;
        }
        .header h1 {
            font-size: 36px;
            margin: 0;
            color: #00695c;
        }
        .header p {
            font-size: 18px;
            color: #00796b;
        }
        .section {
            margin-bottom: 30px;
            padding: 20px;
            background-color: #ffffff;
            border-radius: 8px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
        }
        .section h2 {
            font-size: 24px;
            color: #00695c;
            border-bottom: 2px solid #00695c;
            padding-bottom: 5px;
            margin-bottom: 15px;
        }
        .content {
            margin-left: 20px;
            line-height: 1.8;
            font-size: 16px;
        }
        .content strong {
            color: #333;
            font-weight: bold;
        }
        .experience-item, .education-item, .skill-item {
            margin-bottom: 20px;
        }
        .experience-item p, .education-item p {
            margin-top: 5px;
            color: #555;
        }
        .content ul {
            list-style-type: disc;
            padding-left: 20px;
        }
        .content li {
            margin-bottom: 5px;
        }
        .footer {
            text-align: center;
            margin-top: 40px;
            color: #777;
            font-size: 14px;
        }
        .badge {
            background-color: #4caf50;
            color: white;
            font-size: 14px;
            padding: 4px 10px;
            border-radius: 12px;
            margin-left: 5px;
        }
        .experience-item, .education-item, .skill-item {
            border-left: 4px solid #4caf50;
            padding-left: 10px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="header">
        <h1>${name}</h1>
        <p>${summary}</p>
    </div>

    <div class="section">
        <h2>基本信息</h2>
        <div class="content">
            <strong>出生日期:</strong> ${birthDate} <br>
            <strong>电话:</strong> ${phone} <br>
            <strong>邮箱:</strong> ${email} <br>
            <strong>地址:</strong> ${address} <br>
        </div>
    </div>

    <div class="section">
        <h2>工作经历</h2>
        <#list experiences as experience>
            <div class="content experience-item">
                <strong>${experience.position}</strong> at <strong>${experience.company}</strong> (${experience.startDate} - ${experience.endDate})
                <p>${experience.description}</p>
            </div>
        </#list>
    </div>

    <div class="section">
        <h2>教育背景</h2>
        <#list educations as education>
            <div class="content education-item">
                <strong>${education.schoolName}</strong> - ${education.major} (${education.startDate} - ${education.endDate})
            </div>
        </#list>
    </div>

    <div class="section">
        <h2>技能证书</h2>
        <#list skills as skill>
            <div class="content skill-item">
                ${skill.skillName} <span class="badge">${skill.certificateName}</span>
            </div>
        </#list>
    </div>

    <div class="footer">
        <p>感谢您查看我的简历！</p>
    </div>
</div>
</body>
</html>
