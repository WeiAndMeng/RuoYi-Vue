<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${title}</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            background-color: #f3f3f3;
            margin: 0;
            padding: 0;
            color: #333;
        }
        .container {
            width: 70%;
            margin: 0 auto;
            padding: 40px;
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 5px 20px rgba(0, 0, 0, 0.1);
        }
        .header {
            text-align: center;
            margin-bottom: 40px;
        }
        .header h1 {
            font-size: 36px;
            margin: 0;
            color: #00796b;
        }
        .header p {
            font-size: 18px;
            color: #777;
        }
        .section {
            margin-bottom: 30px;
        }
        .section h2 {
            font-size: 24px;
            color: #00796b;
            border-bottom: 2px solid #00796b;
            padding-bottom: 10px;
            margin-bottom: 15px;
        }
        .content {
            line-height: 1.8;
        }
        .content strong {
            font-weight: bold;
            color: #333;
        }
        .content ul {
            list-style-type: disc;
            padding-left: 20px;
        }
        .content li {
            margin-bottom: 8px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="header">
        <h1>${name}</h1>
        <p>${summary}</p>
    </div>

    <div class="section">
        <h2>工作经历</h2>
        <#list experiences as experience>
            <div class="content">
                <strong>${experience.position}</strong> at <strong>${experience.company}</strong> (${experience.startDate} - ${experience.endDate})
                <p>${experience.description}</p>
            </div>
        </#list>
    </div>

    <div class="section">
        <h2>教育背景</h2>
        <#list educations as education>
            <div class="content">
                <strong>${education.schoolName}</strong> - ${education.major} (${education.startDate} - ${education.endDate})
            </div>
        </#list>
    </div>

    <div class="section">
        <h2>技能证书</h2>
        <#list skills as skill>
            <div class="content">
                ${skill.skillName} (${skill.certificateName})
            </div>
        </#list>
    </div>
</div>
</body>
</html>
