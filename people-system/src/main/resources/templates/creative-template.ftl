<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${title}</title>
    <style>
        body {
            font-family: 'Verdana', sans-serif;
            background-color: #fff;
            margin: 0;
            padding: 0;
            color: #333;
        }
        .container {
            width: 80%;
            margin: 40px auto;
            padding: 30px;
            background-color: #ececec;
            border-radius: 15px;
            box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
        }
        .header {
            text-align: center;
            margin-bottom: 50px;
        }
        .header h1 {
            font-size: 40px;
            color: #2196F3;
            margin-bottom: 10px;
        }
        .header p {
            font-size: 18px;
            color: #777;
        }
        .section {
            margin-bottom: 40px;
        }
        .section h2 {
            font-size: 26px;
            color: #2196F3;
            border-left: 4px solid #2196F3;
            padding-left: 15px;
            margin-bottom: 20px;
        }
        .content {
            line-height: 1.6;
        }
        .content strong {
            font-weight: bold;
            color: #2196F3;
        }
        .content ul {
            list-style-type: square;
            padding-left: 20px;
        }
        .content li {
            margin-bottom: 6px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="header">
        <h1>${name}</h1>
        <p>${summary}</p>
    </div>

    <div class="section">
        <h2>工作经历</h2>
        <#list experiences as experience>
            <div class="content">
                <strong>${experience.position}</strong> at <strong>${experience.company}</strong> (${experience.startDate} - ${experience.endDate})
                <p>${experience.description}</p>
            </div>
        </#list>
    </div>

    <div class="section">
        <h2>教育背景</h2>
        <#list educations as education>
            <div class="content">
                <strong>${education.schoolName}</strong> - ${education.major} (${education.startDate} - ${education.endDate})
            </div>
        </#list>
    </div>

    <div class="section">
        <h2>技能证书</h2>
        <#list skills as skill>
            <div class="content">
                ${skill.skillName} (${skill.certificateName})
            </div>
        </#list>
    </div>
</div>
</body>
</html>
