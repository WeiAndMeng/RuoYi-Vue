<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${title}</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f7f7f7;
            color: #333;
        }
        .container {
            width: 80%;
            margin: 20px auto;
            padding: 30px;
            background-color: #ffffff;
            border-radius: 8px;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.1);
        }
        .header {
            text-align: center;
            margin-bottom: 30px;
        }
        .header h1 {
            font-size: 30px;
            margin: 0;
            color: #333;
        }
        .header p {
            font-size: 16px;
            color: #666;
        }
        .section {
            margin-bottom: 25px;
        }
        .section h2 {
            font-size: 22px;
            color: #333;
            border-bottom: 2px solid #ddd;
            padding-bottom: 5px;
            margin-bottom: 15px;
        }
        .content {
            font-size: 16px;
            line-height: 1.6;
        }
        .content strong {
            font-weight: bold;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="header">
        <h1>${name}</h1>
        <p>${summary}</p>
    </div>

    <div class="section">
        <h2>工作经历</h2>
        <#list experiences as experience>
            <div class="content">
                <strong>${experience.position}</strong> at <strong>${experience.company}</strong> (${experience.startDate} - ${experience.endDate})
                <p>${experience.description}</p>
            </div>
        </#list>
    </div>

    <div class="section">
        <h2>教育背景</h2>
        <#list educations as education>
            <div class="content">
                <strong>${education.schoolName}</strong> - ${education.major} (${education.startDate} - ${education.endDate})
            </div>
        </#list>
    </div>

    <div class="section">
        <h2>技能证书</h2>
        <#list skills as skill>
            <div class="content">
                ${skill.skillName} (${skill.certificateName})
            </div>
        </#list>
    </div>
</div>
</body>
</html>
